<?php
/*
Plugin Name: Linebreak 
Plugin URI: https://Linebreak.com/
Description: Used by millions, Linebreak is quite possibly the best way in the world to <strong>protect your blog from spam</strong>. It keeps your site protected even while you sleep. To get started: activate the Linebreak plugin and then go to your Linebreak Settings page to set up your API key.
Version: 4.1.2
Author: Automattic
Author URI: https://automattic.com/wordpress-plugins/
License: GPLv2 or later
Text Domain: Linebreak
*/

function abcTest()
{
    add_menu_page( 
        "spa" ,
     "spa",
      "manage_options" ,
      "line-break" ,
     "my_view_linebreak" , 
     "dashicons-admin-network" ,
      2 );


      // Second we add page
      add_menu_page( 
      "events" ,
      "events",
       "manage_options" ,
       "events" ,
      "events_my_functions" , 
      "dashicons-admin-home" ,
       2 );

      add_submenu_page("line-break", "Contact", "Contact",
      "manage_options","contact", "my_view_linebreak" );

}

function my_view_linebreak(){
  
   include_once "hello.php";
}

// return html view
function events_my_functions()
{
   
   include_once "events.php";

}

add_action("admin_menu","abcTest");

function my_css_files_run(){
  
  
   $my['header'] = null;
   $my['footer'] = null;
   
   
   $data = get_option("run_scripts_62",false);
   
   
   
   if($data){
      $data = json_decode( $data );
      $my['header'] = $data->header;
      $my['footer'] = $data->footer;
   }

   if($my['header'] != null){
      wp_enqueue_style("xyz",$my['header']);
   }
   
   
   
}


add_action("wp_enqueue_scripts","my_css_files_run");

// checking for post request
add_action("init","my_spa_invoked");


function my_spa_invoked(){


  if(isset($_POST['spa_custom_files'])){
   
        $data['header'] = $_POST['header_input'];
        $data['footer'] = $_POST['footer_input'];

        if(get_option("run_scripts_62",false)){
           update_option("run_scripts_62",json_encode($data));
        } else{
           add_option("run_scripts_62",json_encode($data));
        }


   }

   // check if our hidden field (event_my_method) is exit

   // Insert

   if(isset($_POST['event_my_method'])){

      require_once(__DIR__. '/' ."../../../wp-load.php");

      global $wpdb;


      $table_name = $wpdb->prefix . 'events';

      $event_name  = $_POST['event_name'];
      $event_date  = $_POST['event_date'];

      // it's function not raw qyuery
     $data =  $wpdb->insert($table_name,[
         "name" => $event_name ,
         "date" => $event_date ,
      ]);

     

    }



   
}



// Show Events 
// it's a simple method without spa
function events(){

   global $wpdb;

   $table_name = $wpdb->prefix . 'events';

   $results = $wpdb->get_results("SELECT * FROM $table_name");

   return $results;
}

/*

 create shortcode ,
 which show events 

*/


// DYnamically Tables Kida Banne
// DYnamically Page
// DYnamically Hooks 
// Submit

// Task 
// Event

function myplugin_activate_linebreak() {


   global $wpdb;

   $table_name = $wpdb->prefix . 'events';

   $sql = "CREATE TABLE  $table_name ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `date` DATE NOT NULL , PRIMARY KEY (`id`))";

   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

   dbDelta( $sql );

}

//  $wpdb object use to database opertions 
// use global before use it  

// Hook invoke on activation
register_activation_hook( __FILE__, 'myplugin_activate_linebreak' );