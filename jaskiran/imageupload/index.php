<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
     <div class="container mt-2">
         <div class="form-group">
           <label for="">Upload</label>
           <button class="btn btn-primary" onclick="document.getElementById('image').click()">Upload</button>
           <input type="file" id="image" onchange="handleUpload(this)" style="display:none" multiple>
 
           <span class="helper-text"></span>

           <img src="" alt="" id="output_image" style="width:150px">

           <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progress_bar">
              0%
            </div>
          </div>



         </div>
     </div>


     <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
     <script>
              
               function handleUpload(image){
                    const file = image.files[0];
                    image.nextElementSibling.innerHTML = file.name;
                     
                      output_image =  document.getElementById('output_image');

                      output_image.src =  URL.createObjectURL(file);

                      const form = new  FormData()

                      form.append("image",file )
                      form.append("username", "gurinder" )

                      axios.post("upload.php",form , {
                      
                        onUploadProgress: function (progressEvent) {
                           

                            const  precent = document.getElementById('progress_bar');

                            precent.innerHTML =  `${(progressEvent.loaded / progressEvent.total) * 100}%` ; 

                            precent.style.width = `${(progressEvent.loaded / progressEvent.total) * 100}%`

                        },


                      })


                      .then( (res) => {

                        // console.log(res)

                      })

                      .catch( (e) => {

                          // console.log(e)

                      })


                      
               }
     </script>
</body>
</html>