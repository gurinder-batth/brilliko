<?php
session_start();
// Silence is Golden 
include_once 'conn.php';

function posts(){
global $conn;
$query = "SELECT * FROM posts ORDER BY id DESC";
$posts = mysqli_query($conn,$query);
return $posts;
}

function get_post($id){
    global $conn;
    $query = "SELECT * FROM posts WHERE id = $id";
    $post = mysqli_query($conn,$query);
    return mysqli_fetch_assoc($post);
}

function get_post_comment($id){
    global $conn;
    $query = "SELECT * FROM comments WHERE post_id = $id";
    $comments = mysqli_query($conn,$query);
    return $comments;
}

function recent_posts(){
    global $conn;
    $query = "SELECT * FROM posts ORDER BY id DESC LIMIT 5";
    $posts = mysqli_query($conn,$query);
    return $posts;
    }
    

function random_color(){
    return "#".substr(md5(rand()), 0, 6);
}

function categories(){
    global $conn;
    $query = "SELECT * FROM categories";
    $categories = mysqli_query($conn,$query);
    return $categories;
 }

function login(){
$parms = func_get_args();
extract($parms[0]);
global $conn;
$password = md5($password);
$query = "SELECT * FROM users WHERE email = '$email' AND password = '$password' ";
$rows = mysqli_query($conn,$query);
$row = mysqli_num_rows($rows);

if($row > 0){
    saveUserData(mysqli_fetch_assoc($rows));
    header('Location:index.php');
    exit;
} else {
    session_store('login_error',"Invalid username or password");
    header('Location:login.php'); 
    exit;
}

}

function logout(){
   unset($_SESSION['user']);
   header('Location:index.php');
   exit;
}


function saveUserData($user){
    $_SESSION['user'] = $user;
}

function session_once($var){
    $value =  $_SESSION[$var];
    unset($_SESSION[$var]);
    return $value;
}


function session_store($var,$value){
    $_SESSION[$var] = $value;
}

function session_get($var){
    $value =  $_SESSION[$var];
    return $value;
}

function session_has($var){
    if(isset($_SESSION[$var])){
        return true;
    }
    return false;
}

function isLoginned(){
    
         if(!session_has('user')){
               session_store('login_error',"Please login to continue");
               header('Location:login.php'); 
              exit;
         }

}

function createPost(){
     global $conn;
     $parms = func_get_args();
     extract($parms[0]);
     $image = $_FILES['image'];
     $img_url = 'public/images/'.time().$image['name'];
     move_uploaded_file($image['tmp_name'],$img_url);
     $user_id = session_get('user')['id'];
     $query = "INSERT  INTO posts (title,description,user_id,img) VALUES('$title' ,'$description' ,$user_id ,'$img_url' )";
     mysqli_query($conn,$query);
     session_store('success',"Thank You! Your Post Successfully Submitted");
     header('Location:create_post.php'); 
     exit;
}

function dd($var){
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    exit;
}