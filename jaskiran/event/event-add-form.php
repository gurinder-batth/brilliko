<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <script src="https://www.google.com/recaptcha/api.js?onload=afterCapchta" async defer></script>

</head>
<body>


<div class="container">
  <div class="row">
             <form action="event-add.php" method="post">
                 <div class="form-group">
                     <label for="">Event</label>
                     <input type="text" name="title" class="form-control">
                 </div>
                 <div class="form-group">
                     <label for="">Event Date</label>
                     <input type="date" name="event_at" class="form-control">
                 </div>
                 <div class="form-group">
                 <div class="g-recaptcha" data-sitekey="6LfLK7IUAAAAAOEj0UXyrDpNa3LsTURbsJZkqGq8" data-callback="successCap"></div>
                 </div>

                   <textarea name="key" id="key" cols="30" rows="10" style="display:none"></textarea>

                 <div class="form-group">
                     <button class="btn btn-primary">Submit</button>
                 </div>
             </form>
  </div>
</div>

<script>
function afterCapchta() {
  console.log("cpachta filled")
  }
  
function successCap(data){
  document.getElementById("key").innerHTML = data;
}
</script>

</body>
</html>
