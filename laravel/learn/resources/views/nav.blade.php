<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">Blog</a>
          </div>
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="{{route('post.add.form')}}">AddPost</a></li>
            <li><a href="{{route('post.recent')}}">Recent Post</a></li>
          </ul>
        </div>