<?php

namespace App\Http\Controllers;

use Excel;
use App\Student;
use App\StudentExcel;
use App\StudentImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
   public function index()
   {
    //   $students = DB::table("students")->get();
      $students = Student::get();
      dd($students);
   }

   public function download()
   {
      $students = Student::get();
      $student_excel  = new StudentExcel();
      $student_excel->data = $students;
      return Excel::download($student_excel,"students.xlsx");
   }

   public function import(Request $request)
   {
       Excel::import(new StudentImport, $request->excel);
   }


}
