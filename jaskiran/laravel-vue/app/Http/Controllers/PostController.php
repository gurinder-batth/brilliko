<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function form()
    {
        return view("insert");
    }
    public function insert(Request $request)
    {

        $request->validate([
            "title" =>  'required|min:3|max:255' ,
            "description" =>  'required|min:3|max:255'
        ]);

        // 'applcation/json'

        // create vs insert

        //  fillable (create)
        //  timestamps (create)
        // not fillable (insert)
        // return post model (create)
        // true , false (insert)
      
        $post = Post::create([
            'title' => $request->title ,
            'description' => $request->description 
        ]);

        return response()->json(['status' => true ,'post' =>  $post]);
    }
}
