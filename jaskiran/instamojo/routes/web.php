<?php


Route::get("/","CheckoutController@form");
Route::post("order","CheckoutController@order")->name("order");

Route::get("check-order","CheckoutController@checkOrder")->name("check.order");