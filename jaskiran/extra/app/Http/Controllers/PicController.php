<?php

namespace App\Http\Controllers;

use App\Pic;
use Illuminate\Http\Request;

class PicController extends Controller
{
     public function form()
     {
         return view("upload");
     }


     public function show()
     {
         $pic = Pic::find(1);
        return view("show")->with(['pic' => $pic]);
     }

     public function upload(Request $request)
     {
         $pics = [];
         foreach($request->pics as $pic){
              array_push($pics,$pic->store("pics"));
         }


         Pic::insert(["pics" => json_encode($pics)]);

         return redirect()->back();
         
     }
}
