@extends('master')
@section('content')

<div class="col-md-12">
        <h3>{{$flight->name}}</h3>
</div>

@include('flight.schulde.schulde')

        <div class="col-md-12">
                <form action="{{route('flight.schulde.create')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                                <label for="">Departure From</label>
                                <input class="form-control" name="departure">
                        </div>
            
                            <div class="form-group">
                                    <label for="">Departure At Date</label>
                                    <input  type="date" class="form-control" name="departure_at_date">
                          </div>
                            <div class="form-group">
                                    <label for="">Departure At Time</label>
                                    <input  type="time" class="form-control" name="departure_at_time">
                          </div>

                        <div class="form-group">
                                <label for="">Arrive </label>
                                <input class="form-control" name="arrive">
                        </div>
            
                            <div class="form-group">
                                    <label for="">Arrive At Date</label>
                                    <input  type="date" class="form-control" name="arrive_at_date">
                          </div>

                            <div class="form-group">
                                    <label for="">Arrive At Time</label>
                                    <input  type="time" class="form-control" name="arrive_at_time">
                          </div>
            
                        <input type="hidden" name="flight_id" value={{$flight->id}}>

                            <div class="form-group">
                                    <button class="btn btn-primary">Create Schulde</button>
                          </div>
                  </form>
        </div>

@endsection