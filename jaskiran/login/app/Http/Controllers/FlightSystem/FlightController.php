<?php

namespace App\Http\Controllers\FlightSystem;

use App\Flight;
use App\FlightSchulde;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FlightController extends Controller
{

       //   Post New Flight
       public function flight(Request $request)
       {

            $request->validate([
                'number' => 'required|unique:flights,number' ,
                'name' => 'required'
            ]);

             Flight::create([
                 'name' => $request->name ,
                 'number' => $request->number
             ]);
             return redirect()->route('flight.get');
       }

       public function getFlights()
       {      
            $flights = Flight::get();
            return view('flight.flight.create')->withFlights($flights);
       }

       public function getFlightSchulde($id)
       {
             $flight = Flight::find($id);
             $schuldes = FlightSchulde::whereFlightId($flight->id)->latest()->get();
             return view('flight.schulde.create')->withFlight($flight)->withSchuldes($schuldes);
       }

       //   Post Flight Schulde
       public function createSchulde(Request $request)
       {
            $request->validate([
                'departure' => 'required' ,
                'departure_at_date' => 'required|date' ,
                'departure_at_time' => 'required' , 
                'arrive' => 'required' ,
                'arrive_at_date' => 'required|date' ,
                'arrive_at_time' => 'required' , 
                'flight_id' => 'required'
            ]);
            
               FlightSchulde::create([
                      'departure' => $request->departure ,
                      'departure_at' => $request->departure_at_date .  " " . $request->departure_at_time . ":00" ,
                      'arrive' => $request->arrive ,
                      'arrive_at' => $request->arrive_at_date .  " " . $request->arrive_at_time . ":00" ,
                      'flight_id' => $request->flight_id
               ]);

               return redirect()->back();
       }

       //    Patch Flight Schulde
       public function updateSchulde(Request $request)
       {
            $schulde = FlightSchulde::find($request->id);
               $schulde->update([
                        'departure' => $request->departure ,
                        'departure_at' => $request->departure_at_date .  " " . $request->departure_at_time ,
                        'arrive' => $request->arrive ,
                        'arrive_at' => $request->arrive_at_date .  " " . $request->arrive_at_time,
                        'flight_id' => $request->flight_id
                ]);
               return redirect()->back();
       }

       //    Put Flight Schulde
       public function putSchulde()
       {
           # code...
       }

      //    deleteConfirmation Schulde
      public function deleteConfirmation($id)
      {
          $schulde = FlightSchulde::find($id);
          return view('flight.schulde.delete_confirmation')->withSchulde($schulde);
      }

       //    Delete Flight Schulde
       public function deleteSchulde(Request $request)
       {
           $schulde = FlightSchulde::find($request->id);
           $schulde->delete();
           return redirect()->to($request->previous_url);
       }


        //Get All Flight Schulde
       public function allFlightSchuldes()
       {
           # code...
       }

}
