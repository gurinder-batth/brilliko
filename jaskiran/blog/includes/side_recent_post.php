<nav class="panel">
    <p class="panel-heading">
        Recent Post
    </p>

    <?php $posts = posts(); ?>
    <?php while($post =  mysqli_fetch_assoc($posts)) { ?>
    <a class="panel-block ">

        <span class="panel-icon">
            <i class="far fa-newspaper"></i>
        </span>
        <?= substr($post['title'],0,18) ?>.. &nbsp <span class="quicksand"> <time datetime="2016-1-1">11:09
                PM - 1 Jan 2016</time> </span>
    </a>
    <?php } ?>

</nav>