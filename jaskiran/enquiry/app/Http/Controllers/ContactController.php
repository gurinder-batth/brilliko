<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function add(Request $request)
    {
       $contact =  Contact::create([
               "name" => $request->name
        ]);
       return  response()->json($contact);
    }
}
