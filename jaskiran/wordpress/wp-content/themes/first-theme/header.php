<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> <?= bloginfo('name') ?> </title>
    <?php wp_head() ?>
</head>
<body>


<!-- wp_nav_menu show all the pages which is created by us -->
<?php wp_nav_menu([
    
            "theme_location" => "header_menu" ,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'navbar',
            'menu_class'        => 'nav navbar-nav navbar-left'
           
        ]); ?>

<div class="text-right">
 <?php  get_search_form(); ?> 
</div>
