<?php

namespace App;

use App\Student;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentImport implements ToModel {

    public function model(array $row)
    {
        return new Student([
           'name'     => $row[0],
           'class'    => $row[1],
        ]);
    }

}