<?php

include_once 'Database.php';

class Fake extends Database {

          private $kites = [

                'One' ,
                'Two' ,
                'Three' ,
                'Four' ,
                'Five'

          ];

          private $colors  = [
              'red' ,
              'green' ,
              'black' ,
              'dark black',
              'yellow'
          ];


          public function randomKites()
          {
                 return $this->kites[rand(0,4)];
          }



          public function randomColors()
          {
            return $this->colors[rand(0,4)];
          }



           public function enter()
           {

                
                      $this->connect();
                      // $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                      $db = $this->con->prepare(" INSERT INTO kites (name,color) VALUES(:name,:color) ");
                  
                      for ($i=0; $i <= 50 ; $i++) { 
                    
                         $db->execute([
                                       'name' => $this->randomKites() ,
                                       'color' => $this->randomColors()
                         ]);

                        // print_r ($this->con->errorInfo());
                          
                      }

           }

}