@extends('admin.master')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
            <li class="active">Widgets</li>
        </ol>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Widgets</h1>
        </div>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default articles">
                <div class="panel-heading">
                    Latest News
                    <ul class="pull-right panel-settings panel-button-tab-right">
                        <li class="dropdown"><a class="pull-right dropdown-toggle" data-toggle="dropdown" href="#">
                                <em class="fa fa-cogs"></em>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <ul class="dropdown-settings">
                                        <li><a href="#">
                                                <em class="fa fa-cog"></em> Settings 1
                                            </a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">
                                                <em class="fa fa-cog"></em> Settings 2
                                            </a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">
                                                <em class="fa fa-cog"></em> Settings 3
                                            </a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <span class="pull-right clickable panel-toggle panel-button-tab-left"><em
                            class="fa fa-toggle-up"></em></span></div>
                <div class="panel-body articles-container">
                   <table class="table">
                       <tr>
                           <th>Sr</th>
                           <th>Name</th>
                           <th>Date</th>
                           <th>Message</th>
                           <th> <i class="fa fa-info"></i> </th>
                       </tr>
                       @foreach ($lastfollowups as $i => $lf)
{{--  --}}
                        <tr>
                            <td>{{$i+1}}</td>
                            <td>{{$lf->followup->enquiry->name}}</td>
                            <td>{{$lf->follow_at->format("d-M-y")}}</td>
                            <td>{{$lf->followup->description}}</td>
                            <td>
                               <a href="{{route('enquiry.enquiry',['id' => $lf->enquiry_id])}}"> 
                                 <i class="fa fa-info"></i>
                                </a>
                          </td>
                        </tr>
                           
                       @endforeach
                   </table>
                </div>
            </div>
            <!--End .articles-->

        </div>
        <!--/.col-->

        <div class="col-sm-12">
            <p class="back-link">Lumino Theme by <a href="https://www.medialoot.com">Medialoot</a></p>
        </div>
    </div>
    <!--/.row-->
</div>
<!--/.main-->
@endsection
