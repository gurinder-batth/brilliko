<?php

Route::get("/","StudentController@index");
Route::get("download","StudentController@download");
Route::post("import","StudentController@import")->name("import.excel");
Route::view("import","import");