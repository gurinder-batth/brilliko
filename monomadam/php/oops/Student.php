<?php




class ListHelper {

         private $collection = [];

         public function create($val)
         {
                 array_push($this->collection,$val);
         }

         public function collection()
         {
             return $this->collection;
         }


}

class Student extends ListHelper {


            public function list(){
                return $this->collection();
            }

            public function add($name)
            {
                $this->create($name);
            }

}

$student = new Student();
$student->add("Gurinder");
$student->add("Sanjeev");
$student->add("ABC");

print_r($student->list());