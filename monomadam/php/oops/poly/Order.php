<?php

interface Logger {

    public function send();

}

class SmsLog implements Logger{

      public function send(){
             echo "order placed (SMS)";
      }

}


class EmailLog  implements Logger {

    public function send(){
           echo "order placed (Email)";
    }

}

class Order{

    public function placed(Logger $log)
    {
             $log->send();
    }

}

$order = new Order();
$log = new SmsLog();
// $log = new EmailLog();
$order->placed($log);