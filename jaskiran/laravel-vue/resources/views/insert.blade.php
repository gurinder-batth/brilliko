@extends('master')

@section('content')

 <div class="container" >
     <form @submit.prevent="insert" v-cloak>
         <div class="alert alert-success" v-if="message">
             <strong>
                 @{{message}}
             </strong>
         </div>
         <div class="form-group">
              <label for="">Title</label>
              <textarea name="" v-model="post.title" class="form-control" id="" cols="30" rows="2"></textarea>
              <span v-if="errors.title" class="text-danger"> @{{errors.title}} </span>
         </div>
         <div class="form-group">
              <label for="">Description</label>
              <textarea name="" v-model="post.description" class="form-control" id="" cols="30" rows="2"></textarea>
              <span v-if="errors.description"> @{{errors.description}} </span>
            </div>
         <div class="form-group">
             <button class="btn btn-primary">Submit</button>
         </div>
     </form>
 </div>

@endsection