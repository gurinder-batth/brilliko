<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightSchulde extends Model
{

    protected $fillable = ['departure','departure_at','arrive','arrive_at','flight_id'];
}
