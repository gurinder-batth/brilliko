<?php

use App\Post;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {


    $user = User::find(12);

    // Find It's Post

     //Without Relation Ship
    // $posts = Post::whereUserId($user->id)->get();
    
     //With Relation Ship
    $posts = $user->posts;



    // Collection of Post Model
    foreach($posts as $post){

        // Retieve Post
        // dump($post->user);

        // Retieve COmments Model from Post Model

        foreach($post->comments as $comment){
             dump($comment->reply);
        }
     

            //   $user = User::find($post->user_id);
            //    dd($user);
    }
});
