<?php
session_start();
include_once 'function.php';
isLogined();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container"> 

        <div class="jumbotron">
                      <span class="h3"> Hello! <?= $_SESSION['user']['name'] ?> </span>
                      <span><a href="logout.php">Logout</a></span>
        </div>

</div>

</body>
</html>

