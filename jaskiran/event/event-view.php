<?php
use Carbon\Carbon;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Events</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
          <div class="container">
          
              <div class="row">
               
               <!-- New Events -->
                <div class="col-md-6">
                     <?php foreach($newres as $event) { ?>

                         <h1> <?= $event['title'] ?> </h1>

                         <?php $carbon = new Carbon($event['event_at']); ?>
                         <time> <?= $carbon->toFormattedDateString() ?> </time>

                     <?php } ?>
                </div>


                <!-- Old Events -->
                <div class="col-md-6">
                    <?php foreach($oldres as $event) { ?>

                        <h1> <?= $event['title'] ?> </h1>

                        <?php $carbon = new Carbon($event['event_at']); ?>
                         <time> <?= $carbon->toFormattedDateString() ?> </time>

                     <?php } ?>
                </div>
               
              </div>
          
          </div>
</body>
</html>