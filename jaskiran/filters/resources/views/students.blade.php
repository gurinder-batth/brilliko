
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 

</head>
<body>

    <div id='app'>
  
            <div class="container">

                    <form action="{{route('students.list')}}" class="mt-2 mb-2" name="filter_form">
                     {{$students->total()}}
                       <div class="row">
                           <div class="col-md-3">
                               <select name="course_id" id="" class="form-control" onchange="document.filter_form.submit()">
                                     <option value="all">All</option> 
                                      @foreach ($courses as $course)
                                       <option value={{$course->id}} 
                                        @if(request()->get('course_id') == $course->id)
                                            selected="selected"
                                        @endif
                                        >
                                         {{$course->name}}</option>
                                      @endforeach
                               </select>
                           </div>
                           <div class="col-md-3">
                               <select name="batch_id" id="" class="form-control" onchange="document.filter_form.submit()">
                                     <option value="all">All</option> 
                             
                                     @foreach ($batchs as $batch)
                                     <option value={{$batch->id}} 
                                      @if(request()->get('batch_id') == $batch->id)
                                          selected="selected"
                                      @endif
                                      >
                                      {{$batch->name}}</option>
                                    @endforeach
                               </select>
                           </div>
                           <div class="col-md-3">
                               <select name="admission" id="" class="form-control" onchange="document.filter_form.submit()">
                                       <option value="all">All</option>
                                       <option value="1"   
                                            @if(request()->get('admission') == '1')
                                             selected="selected"
                                             @endif>Admit</option>
                                       <option value="0"
                                       @if(request()->get('admission') == '0')
                                       selected="selected"
                                       @endif>Pending</option>
                               </select>
                            
                           </div>
                           <div class="col-md-3">
                                 <select name="marks" id="" class="form-control" onchange="document.filter_form.submit()">
                                         <option value="all">All</option>
                                         <option value="1"   
                                              @if(request()->get('marks') == '1')
                                               selected="selected"
                                               @endif>Pass</option>
                                         <option value="0"
                                         @if(request()->get('marks') == '0')
                                         selected="selected"
                                         @endif>Fail</option>
                                 </select>
                           </div>
                       </div>
             
                    </form>
             
                    <table class="table">
                        <tr>
                            <th>Sr</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Course</th>
                            <th>Batch</th>
                            <th>Admission</th>
                            <th>Marks</th>
                        </tr>
                        @foreach ($students as $i => $student)
                            <tr>
                                <td>{{$i+1}}</td>
                                <td 
                                     data-target="#myModal" 
                                     data-toggle="modal"
                                     @click="updateModalData('{{$student}}')"
                                     >{{$student->name}}</td>
                                <td>{{$student->phone}}</td>
                                <td>{{$student->course->name}}</td>
                                <td>{{$student->batch->name}}</td>
                                <td>
                                     @if($student->admission)
                                       <i class="fa fa-check text-success"></i>
                                     @else
                                     <i class="fa fa-times text-danger"></i>
                                     @endif
                               </td>
                               <td>{{$student->marks}}</td>
                            </tr>
                        @endforeach
                    </table>
                    {{$students->appends(request()->all())->links()}}
             </div>

@include('modal')
    </div>


    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
           
           data:null,

           
        } ,
        methods:{

                updateModalData(student){
                      this.data = JSON.parse(student)
                } ,
                update(){

                      let data = this.data;

                      axios.post("{{route('students.update')}}",{
                          data
                      })

                      .then( (res) => {
                          console.log(res)
                      } )

                      .catch((e) => {
                          console.log(e.response.data)
                      })
                }

        }
    })
</script>

</body>
</html>
