<?php

namespace App;

use Illuminate\Foundation\Auth\User ;

class Admin extends User
{
   protected $fillable = ['mobile','password','email'];
}
