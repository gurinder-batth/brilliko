<?php
namespace Library ;

class DefaultConfiguration {
    protected $method  = "smartphones_method";
    protected $prefix_url = "smartphones-";
    protected $icon = "dashicons-smartphone";
    protected $position = 2;
    public $plugin_path = WP_PLUGIN_DIR . '/' . 'smartphones/';
    public $plugin_url = WP_PLUGIN_URL . '/' . 'smartphones/';
}

