<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use DB;

class ContactController extends Controller
{
    

    public function index()
    {
       return view('contact');
    }

    public function save(Request $request)
    {

        $request->validate([
          'name' => 'required|min:3|max:30' ,
            'email' => 'required|email|min:5|max:30' ,
            'message'=> 'required|min:3|max:255'
        ]  ,
        [
            'name.required' => 'Oops! You forget to fill :attribute field'
        ]);

         $status = Contact::insert(
            [
                 'name' => $request->name ,
                 'email' => $request->email ,
                 'message' => $request->message
            ]);

        session()->flash('sucesss','Thank you! for connecting with us.');
        return redirect()->back();

    }

    public function list()
    {
       $contacts  = Contact::orderBy('id','DESC')->paginate(2);
       return view('contact_list')->withContacts($contacts);
    }

}

