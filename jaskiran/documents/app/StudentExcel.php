<?php

namespace App;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Student;

class StudentExcel implements FromCollection {

    public $data;
    public function collection()
    {
        return $this->data;
        // return a collection var
    }

}