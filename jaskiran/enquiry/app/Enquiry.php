<?php

namespace App;

use App\FollowUp;
use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
   protected $fillable = ['mobile' , 'name' , 'description', 'status'];

   public function followups()
   {
      return $this->hasMany(FollowUp::class,"enquiry_id");
   }


   public function students()
   {
        $students = [
               [
                   'name' => 'Gurinder'
               ],
               [
                   'name' => 'Jaskiran'
               ],
           ];
        return response()->json($students);
   }
}
