<?php

class Students {


    public function get(){
        echo "get method" . "<br>";
        return $this;
    }

    public function orderBy(){
        echo "order By" . "<br>";
        return $this;
    }

    public function where(){
        echo "where" . "<br>";
        return $this;
    }



}

$students  = new Students();
$students->orderBy()->where()->get();