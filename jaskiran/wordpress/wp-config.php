<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

define('FS_METHOD', 'direct');
/** MySQL database username */
define( 'DB_USER', 'user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in douhttp://appriseconsultant.com/wordpress/2019/08/03/hi-dummy-post/http://appriseconsultant.com/wordpress/2019/08/03/hi-dummy-post/bt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4IC@v[;e5*JI0$cs3,pf!0pV>gt2 rK;=2^ULj81GMO%|d$Pg,K6=t3<JiH*/9|{' );
define( 'SECURE_AUTH_KEY',  'kHKeM03}5hr_#$7scC0dERUh%FS*^i3Y1C[]pe[65&RgxksA0[Fa0odLlK&:rZ22' );
define( 'LOGGED_IN_KEY',    ',KF80T?^Cac;+[A9:QQ %x^u HMAR)B7e$d97~~i?7*n^KJ3B:E0aZUVA?^aP4pb' );
define( 'NONCE_KEY',        'oj=jhD[ohDE8Q-s8nNUKkMWzas/]}koY iA!ouWUo;yH:%(gVa{>>OS9n8=0Pd:n' );
define( 'AUTH_SALT',        '+#A$h/q%1OUf*k;O##x=)v4~mO(Y+@^_6x!R-s)1Dhe(I6e4JbUuK=Kh>X@l+T0*' );
define( 'SECURE_AUTH_SALT', 'UbWGK>7B:tVWLYWb:hBAB&*Jv+SJ-/s>uioExHJEUsS,doTPUW88j67HojQ%co^t' );
define( 'LOGGED_IN_SALT',   ')L.{sGGAfaO:(b#3m$xNCt7ok/]![z tnJgJ:q8TBoy#IK4gX6?_z#wxF/o@(v8`' );
define( 'NONCE_SALT',       'I}VGo>P-a0{4N6=O3^ySiptCx$?xvf~J0k1s/)|>b[TJ,N%+vCB]$s<1c>Ebf+;z' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
