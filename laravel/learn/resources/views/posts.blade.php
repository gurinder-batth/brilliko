@extends('master')

@section('title')
     Recent Post
@endsection

@section('css')
     <link rel="stylesheet" href="style.css">
@endsection


@section('content')


<div class="container">

    @if(Auth::check())


     <img src="{{URL("storage/app/".Auth::user()->pic)}}" alt="">

     {{Auth::user()->name}}

     <a href="{{route('logout')}}">Logout</a>

    @endif

    <a href="{{route('post.add.form')}}">Add Post</a>

    @foreach ($posts as $post)

          <div class="panel">
              <div class="panel-heading">
              <h3> {{$post->title}} <a href="{{route("post.edit" , [ "id" => $post->id ] )}}">Edit</a>  <a href="{{route("post.delete" , [ "id" => $post->id ] )}}">Delete</a>  </h3>
              </div>
              <div class="panel-body">
                  <p> {{$post->description}} </p>
              </div>
              <div class="panel-footer">
                  <p> {{$post->created_at}} </p>
              </div>
          </div>

    @endforeach

      {{$posts->links()}}
</div>

@endsection
