<?php $posts = posts(); ?>
<?php while($post =  mysqli_fetch_assoc($posts)) { ?>
<div class="content">
    <figure class="image is-128x128" style="float:left;margin:1rem">
        <img class="" src="<?= $post['img'] ?>" alt="" width="20%">
    </figure>
    <span class="title quicksand  is-size-4">
        <?= $post['title'] ?></span>
    <span class="subtitle quicksand is-size-6">
        <?= substr($post['description'],0,250) ?>.....</span>
    <span class=""> <a href="single_post.php?post_id=<?= $post['id'] ?>">Read More</a> </span> <br>
    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
</div>
<div style="clear:both"></div>
<?php } ?>