@extends('master')

@section('content')

          @if(Auth::guard('admin')->check())
          <h1>Welcome Sir {{Auth::guard('admin')->user()->mobile}}  <a href="{{route('admin.logout')}}">Logout</a></h1>
          @else
          <h1>Please Login to Go for <a href="{{route('admin.login')}}">  Admin Panel </a></h1>
          @endif

@endsection