<?php
session_start();

function connect(){
    return mysqli_connect("localhost","user","password","school-amrit");
}

function clean_input($val){
    $val = trim($val); 
    $val = htmlspecialchars($val);
    $val = stripslashes($val); 
    return $val;
}

function redirect($page){
    header("Location:$page");
    exit;
}



function create_session($name,$val){
    $_SESSION[$name] =  $val;
}

function retrieve_session($name){
   return $_SESSION[$name];
}

function retrieve_session_once($name){
   $temp_val = $_SESSION[$name];
   unset($_SESSION[$name]);
   return $temp_val;
}

function has_session($name){
    if(isset($_SESSION[$name])){
        return true;
    }
    return false;
}


function storeInDB($query){
   $conn = connect();
   $status = mysqli_query($conn,$query);
   if($status){
       return $status;
   } else{
       echo mysqli_error($conn);
       exit;
   }
}

function  contact_handle(){

    validate();

 
    $name = clean_input($_POST['name']);
    $mobile = clean_input($_POST['mobile']);
    $email = clean_input($_POST['email']);
    
    storeInDB("INSERT INTO contact (name,email,mobile) VALUES('$name','$email','$mobile')");
    
    redirect("index.php");
}



function validate(){
 
 $name = clean_input($_POST['name']);
 $mobile = clean_input($_POST['mobile']);
 $email = clean_input($_POST['email']);

 if(strlen($name) < 3 || strlen($name) > 10){
     create_session("error","Name should be greater than 3 chars and less than 10 chars");
     redirect("index.php");
 }

 if(strlen($mobile) != 10){
     create_session("error","Mobile number should be 10 chars");
     redirect("index.php");
 }

 if(strlen($email) == null){
    create_session("error","Email Required");
    redirect("index.php");
}


}