<?php

namespace App;

use Illuminate\Http\Request;

class Filter {

    public $req;

     public function __construct(Request $request)
     {
         $this->req = $request;
     }

     public function apply($model)
     {
         $model = $model->where($this->removeFields());
         $model = $this->marksFilter($model);
         return $model;
     }

     public function marksFilter($model)
     {
          if($this->req->get('marks') != "all"){
             if($this->req->get('marks') == 1){
                return $model->where("marks",">=",33);
             }
             if($this->req->get('marks') == 0){
                return $model->where("marks","<",33);
             }
          }
          return $model; 
     }


     public function removeFields()
     {

        // return  $this->req->all();

         $data = [];

         //$name = name of field parms
         //$value = value of field parms

         $ignore_fields  = ['page','marks'];

         $fields = $this->req->except($ignore_fields);

         foreach($fields as $name => $value){
                if($value != "all"){
                    // $data['course_id'] => 3
                    $data[$name] =$value;
                }
         }

         return $data;
     }


}