<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>File Uploading</title>
    <link rel="stylesheet" href="{{URL('public/css/app.css')}}">
</head>
<body>
            <div class="container" style="margin-top:3rem">
                   <form action="{{route('photo.upload')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="form-group">
                                <label for="">File</label>
                                <input type="file" name="image">
                           </div>
                           <div class="form-group">
                               <button class="btn btn-primary">Submit</button>
                           </div>
                   </form>
            </div>
</body>
</html>