<?php

namespace App\Http\Controllers;

use App\Enquiry;
use App\LastFollowUp;
use Illuminate\Http\Request;
use App\Http\Requests\EnquiryRequest;
use App\Http\Controllers\Helpers\EnquiryHelper;

class EnquiryController extends Controller
{

    use EnquiryHelper;

    public function __construct(Request $request)
    {
        $this->req = $request;
    }

    public function addForm()
    {
        return view("admin.enquiry.add");
    }

    public function addEnquriy(EnquiryRequest $request)
    {
        $enquiry = $this->addNewEnquiry();
        $followup = $this->createNewFollowUp($enquiry);
        $this->createLastFollowUp($followup);
        return redirect()->back();
    }

    public function list()
    {
       $enquires = Enquiry::latest()->get();
       return view("admin.enquiry.list")->withEnquires($enquires);
    }

    public function followups()
    {
        $lastfollowups = LastFollowUp::whereDate("follow_at","<=",now()->toDateString())
                                        ->whereEnquiryStatus(1)
                                        ->paginate(20);
                  
       return view("admin.enquiry.followups")->withLastfollowups($lastfollowups);                      
    }

    public function enquiry()
    {
          //  Happy RHo
         $enquiry = Enquiry::find($this->req->id);
         return view("admin.enquiry.single")->withEnquiry($enquiry);
    }

    public function addFollowUp()
    {
        $enquiry = Enquiry::find($this->req->enquiry_id);
        $followup = $this->createNewFollowUp($enquiry);
        $this->createLastFollowUp($followup);
        return redirect()->back();
    }


    public function students()
    {
         $students = [

            [
                'name' => 'Jaskiran'
            ],
            
                [
                    'name' => 'Gurinder'
                ],
              
            ];
         return response()->json($students);
    }

    
}
