<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = "my_students";

    public $timestamps = false;

    protected $fillable = ['name','class'];
}
