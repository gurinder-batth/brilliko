<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

   <h1 id="title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis aliquid voluptatum a! Maiores atque corporis quas quisquam aspernatur fugiat obcaecati, ipsum animi odit laboriosam, sunt minus, sapiente est? Rerum, praesentium!</h1>


 <button class="btn btn-primary" id="btn">Hide ME</button>

 <button class="btn btn-primary" id="btn2">Show ME</button>

 <button class="btn btn-primary" id="btn3">Toggle</button>

    <script src="my.js"></script>
</body>

</html>