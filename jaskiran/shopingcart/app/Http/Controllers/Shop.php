<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class Shop extends Controller
{

     
     public function home()
     {
         $products = Product::latest()->get();
          return view("frontend.home")->withProducts($products->chunk(3));
     }


}
