<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class Search extends Controller
{
       public function main(Request $request)
       {
             
            $students = Student::where('fname','LIKE','%'.$request->str.'%')
                                       ->get();

            return response()->json($students);

       }
}
