<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Course;
use App\Filter;
use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{

    public $filter;

    public function __construct(Request $request)
    {
        $this->filter = new Filter($request);
    }

    public function get(Request $request,Student $student)
    {
       $courses = Course::get();
       $batches = Batch::get();
       $student = $this->filter->apply($student);
       $students = $student->paginate(10);
       return view("students")
                  ->withStudents($students)
                  ->withCourses($courses)
                  ->withBatchs($batches);
    //    return view("students",
    //                ['students' => $students] ,
    //                ['courses' => $courses] ,
    //                ['batchs' => $batches] 
    //         );
    }


    public function update(Request $request)
    {
        $student = Student::find($request->data['id']);
        $student->update($request->data);
        return response()->json(['status' => 'done']);
    }
}
