@extends('master')

@section('content')

@if($errors->any())
       <div class="alert alert-danger">
             {{$errors->first()}}
       </div>
@endif

@if(session()->has('unsuccess'))
<div class="alert alert-danger">
        {{session()->get('unsuccess')}}
  </div>
@endif

@if(session()->has('success'))
<div class="alert alert-success">
        {{session()->get('success')}}
  </div>
@endif


<div class="container">
 <form action="{{route('password.reset')}}" method="post">

    {{ csrf_field() }}
     <div class="form-group">
         <label for="">Password</label>
         <input type="password" class="form-control" name="password">
     </div>
     <div class="form-group">
         <label for="">Password Confirmation</label>
         <input type="password" class="form-control" name="password_confirmation">
     </div>

     <input type="hidden" value="{{$token}}" name="token">
     <input type="hidden" value="{{$id}}" name="id">

     <div class="form-group">
       <button class="btn btn-primary">Reset Password</button>
    </div>

</form>
</div>

@endsection