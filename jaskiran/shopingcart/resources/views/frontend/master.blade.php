<!DOCTYPE html>
<html lang="en">
<head>
  <title>ShopToday</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet"> 
  <style>
  .quicksand{
    font-family: 'Quicksand', sans-serif;
  }
  .cart{
    display: none;
  }
  .product{
    transition: all ease-out .2s;
  }
  .product:hover  {
         transform: scale(1.3);

  }
  .panel-body:hover .cart{
    display: block;
  }
  </style>
</head>
<body>

    @yield('content')

  

</body>
</html>
