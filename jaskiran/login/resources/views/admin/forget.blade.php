@extends('master')

@section('content')

@if($errors->any())
       <div class="alert alert-danger">
             {{$errors->first()}}
       </div>
@endif

@if(session()->has('unsuccess'))
<div class="alert alert-danger">
        {{session()->get('unsuccess')}}
  </div>
@endif

@if(session()->has('success'))
<div class="alert alert-success">
        {{session()->get('success')}}
  </div>
@endif


<div class="container">
 <form action="{{route('password.forget')}}" method="post">

    {{ csrf_field() }}
     <div class="form-group">
         <label for="">Email:</label>
         <input type="email" class="form-control" name="email">
     </div>

     <div class="form-group">
       <button class="btn btn-primary">Reset Password</button>
    </div>

</form>
</div>

@endsection