@extends('frontend.master')
@section('content')

<div class="container" style="margin-top:2rem">
    <div class="row" style="background:rgb(245,245,245);padding:4rem">
        @foreach ($products as $chunks)
        @foreach ($chunks as $product)
        <div class="col-md-4 text-center">
            <div class="panel " >

                <div class=" panel-body">
                       <img src="{{$product->images}}" class="product" alt="" srcset="" height="250px"> 
                       <h4 class="quicksand">{{$product->name}}</h4>
                       <div class="cart">
                              <h1>Cart</h1>
                       </div>
                </div>

            </div>
        </div>
        @endforeach
        @endforeach
    </div>
</div>

@endsection
