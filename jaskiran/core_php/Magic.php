<?php

function uncamelize($camel,$splitter="_") {
    $camel=preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter.'$0', $camel));
    return strtolower($camel);

}


class Magic {

    public function  __call($name, $arguments) {
      
        if(substr($name,0,4) == "with"){

           $col = substr($name,4);

           $col = uncamelize($col);

           $id = $arguments[0];

           "SELECT * FROM ABC WHERE $col = $id";

        }else {
            echo "undefined method";
        }


    }

    public function test()
    {
        echo "Hi i AM Test";
    }


}

$magic = new Magic();
echo $magic->withUserId(5);