<?php
namespace Library;

use Library\MenuPages;
use Library\DefaultConfiguration;
use App\Assets;
class PublicAssets extends DefaultConfiguration{

     function __construct(){
        $this->assets  = new Assets();
        $this->assets->registerCss();
        $this->assets->registerJs();
        $this->loadAssets();
     }

     public function loadAssets()
     {
        $this->css();
        $this->js();
     }

     // Load The Css File Which Write By developer in Assets Class
     public function css()
     {
         foreach($this->assets->cssFiles as $file){
            extract($file);
             wp_enqueue_style($this->prefix_url . $name , $this->plugin_url . 'public/css/' . $name );
        }
     }

     // Load The Javascript File Which Write By developer in Assets Class
     public function js()
     {
         foreach($this->assets->jsFiles as $file){
            extract($file);
            wp_enqueue_script($this->prefix_url  . $name , $this->plugin_url . 'public/js/' . $name , '' , '', true);
        }
     }



}