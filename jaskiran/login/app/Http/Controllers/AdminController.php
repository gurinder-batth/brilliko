<?php

namespace App\Http\Controllers;

use App\User;
use App\Admin;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\RegisterVerification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{


    public function dashboard()
     {
         return view('admin.dashboard');
     }
     
     public function loginForm()
     {
        return view('admin.login');
     }

     public function registerForm()
     {
        return view('admin.register');
     }

     public function login(Request $request)
     {
                 $status  = Auth::guard('admin')->attempt([
                                      'mobile' => $request->mobile ,
                                      'password' => $request->password   ,
                                      'active' => 1
                                    ]);
                if($status) {
                    return redirect()->route('admin.dashboard');
                }
                session()->flash('unsuccess',"invalid mobile & password");
                return redirect()->back();
     }

     public function register(Request $request)
     {
            $request->validate([
                'mobile' => 'required|min:10|max:10|unique:admins,mobile' ,
                'password' => 'required|min:3|max:16' ,
                'email' => 'required|min:5|max:30|unique:admins,email' ,
            ]); 

            $admin  =  Admin::create([
                'mobile' => $request->mobile ,
                'password' => bcrypt($request->password) ,
                'email' => $request->email
            ]);

            $admin->token = Str::random(60);
            $admin->save();
            // $registerVerification = new RegisterVerification();
            // Mail::to($registerVerification);
            Mail::to($admin->email)->send(new RegisterVerification($admin));
            session()->flash("success","You'r successfully registered with us. Please your check email for activation your account!");
            return redirect()->route('admin.login.form');

     }

    public function verification(Request $request)
    {
             $admin = Admin::find($request->id);
        
             if($admin->token == null){
                 abort(404);
             }

             if($admin->token == $request->token){
                 $admin->active = 1;
                 $admin->token = null;
                 $admin->save();
                //  session()->flash("success","You'r Account Successfully Activated!");
                //  return redirect()->route('admin.login.form');

                // IF Person Successfully Verified Please login him/her directly
                 Auth::guard('admin')->loginUsingId($admin->id);
                 return redirect()->route('admin.dashboard');
             }

             abort(404);
    }


     public function logout()
     {
         Auth::guard('admin')->logout();
         return $this->dashboard();
     }

     public function websiteUsers()
     {
        
        $users = User::get();
        $admins = Admin::get();


        return view('websites-users')->withUsers($users)->withAdmins($admins);
        
     }

}
