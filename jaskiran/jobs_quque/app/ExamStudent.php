<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamStudent extends Model
{
    protected $table = "exam_students";

    public function student()
    {
        return $this->hasOne(Student::class,"id","student_id");
    }
}
