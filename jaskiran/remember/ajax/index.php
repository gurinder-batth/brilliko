<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ajax</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.css">
</head>
<body>
    

   <section class="section">
    <div class="container">
                     <button class="button is-link" onclick="load_students()">Click Me :)</button>

                     <p id="message" class="has-text-info"></p>

                     <div id="students">
                     
                     </div>
    </div>
   </section>



<script src="ajax.js"></script>
</body>
</html>