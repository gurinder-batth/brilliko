<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jquery</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<!-- 
<p id="message">Hello World!</p> -->
    
<!-- 
<button>Click Me</button>
<button>Click Me 2</button>
<button>Click Me 3</button>
<button>Click Me 4</button>
<button>Click Me 5</button> -->

<!-- 
<button id="hide_btn">Click Me to Hide</button> -->


<!-- <div class="box"></div>

<div class="box_rel">
    <div class="box_absolute"></div>
</div> -->
<!-- 
<div class="box_fixed"></div> -->

<!-- <h1>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, amet distinctio blanditiis nobis laudantium perspiciatis ullam dolorum natus unde veniam porro illo nemo, quam itaque, sunt incidunt accusantium corporis velit!</h1>
<h1>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, amet distinctio blanditiis nobis laudantium perspiciatis ullam dolorum natus unde veniam porro illo nemo, quam itaque, sunt incidunt accusantium corporis velit!</h1>
<h1>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, amet distinctio blanditiis nobis laudantium perspiciatis ullam dolorum natus unde veniam porro illo nemo, quam itaque, sunt incidunt accusantium corporis velit!</h1>
<h1>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, amet distinctio blanditiis nobis laudantium perspiciatis ullam dolorum natus unde veniam porro illo nemo, quam itaque, sunt incidunt accusantium corporis velit!</h1>
<h1>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, amet distinctio blanditiis nobis laudantium perspiciatis ullam dolorum natus unde veniam porro illo nemo, quam itaque, sunt incidunt accusantium corporis velit!</h1>
<h1>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, amet distinctio blanditiis nobis laudantium perspiciatis ullam dolorum natus unde veniam porro illo nemo, quam itaque, sunt incidunt accusantium corporis velit!</h1>
<h1>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, amet distinctio blanditiis nobis laudantium perspiciatis ullam dolorum natus unde veniam porro illo nemo, quam itaque, sunt incidunt accusantium corporis velit!</h1> -->


<div class="box-animate"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="my.js"></script>
</body>
</html>