@extends('master')

@section('content')


@if($errors->any())
       <div class="alert alert-danger">
             {{$errors->first()}}
       </div>
@endif

@if(session()->has('unsuccess'))
<div class="alert alert-danger">
        {{session()->get('unsuccess')}}
  </div>
@endif




@if(session()->has('success'))
<div class="alert alert-success">
        {{session()->get('success')}}
  </div>
@endif


<form action="{{route('otp.send')}}" method="post">
    {{ csrf_field() }}

    <div class="form-group">
            <label for="">Email</label>
            <input class="form-control" name="email">
    </div>

    <div class="form-group">
     <button class="btn btn-primary">Submit</button>
</div>


</form>

@endsection