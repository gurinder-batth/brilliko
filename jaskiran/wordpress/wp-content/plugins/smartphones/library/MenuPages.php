<?php
namespace Library;
use App\AdminPage;
class MenuPages extends DefaultConfiguration {

      public function  __construct()
      {
         add_action('admin_menu',[$this,'register']);
      }

      // Register Method Use to Register Stuff
       public function register()
       {
         $this->adminPage = new AdminPage();
         $this->createPage();
         $this->createSubPage();
       }
       
       // Create Page Method Iterative over the Page Property to Register the Admin pages 
        public function createPage(){
            foreach($this->adminPage->page as $page){
              // Assign the default value of Icon and Position if Developer not Define in adminPage 
              // Property of AdminPage Class
              $icon = $this->icon;
              $position = $this->position;
              // Extract The Array of $page @var
              extract($page);
              add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function,$icon,$position);
            }
       }

       // Create SubPage Method Iterative over the Page Property to Register the Admin sub_pages 
        public function createSubPage(){
            foreach($this->adminPage->sub_page as $page){
              // Assign the default value of Icon and Position if Developer not Define in adminPage 
              // Property of AdminPage Class
              $icon = $this->icon;
              $position = $this->position;
              // Extract The Array of $page @var
              extract($page);
              add_submenu_page($parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);
            }
       }


}
