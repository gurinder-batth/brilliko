<?php require_once 'includes/header.php'; ?>

<div class="container" style="margin-top:3rem">

     <?php  if(has_session("error")) { ?>

      <div class="alert alert-danger">
        <strong> <?= retrieve_session_once("error") ?> </strong>
      </div>

     <?php } ?>

    
    <form action="request.php" method="post">
    <div class="form-group">
         <label for="">Name</label>
         <input type="text" name="name" id="" class="form-control">
        </div>
        <div class="form-group">
         <label for="">Mobile</label>
         <input type="tel" name="mobile" id="" class="form-control">
        </div>
        <div class="form-group">
         <label for="">Email</label>
         <input type="email" name="email" id="" class="form-control">
        </div>
        <input type="hidden" name="method" value="contact_handle">
        <div class="form-group">
             <button class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>

<?php  require_once 'includes/footer.php'; ?>