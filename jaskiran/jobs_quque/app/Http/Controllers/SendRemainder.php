<?php

namespace App\Http\Controllers;

use App\Jobs\MailJob;
use App\Mail\RemainderMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendRemainder extends Controller
{
    public function do()
    {  
        /*
         php artisan queue:table
         php artisan table
         php artisan queue:work 
         Make Job MailJob
 Try this with database
              QUEUE_CONNECTION=database

        */
        MailJob::dispatch("gurinder@domainandspace.com")->delay(now()->addMinutes(1));
        
        /* 
              Try this without database
              QUEUE_CONNECTION=sync
        // MailJob::dispatch("gurinder@domainandspace.com");
        */
      
        /*
        First Step Try this  
        Mail::to("gurinder@domainandspace.com")->send(new RemainderMail("Gurinder"));
       */
    }

    /*
      php artisan queue:table
         php artisan migrate
         php artisan queue:work 
        */
}
