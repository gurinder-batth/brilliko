<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
class PhotoController extends Controller
{
        public function upload(Request $request)
        {


                if($request->hasFile('image')){

                    // $image = $request->image;
                    // $original_name = $image->getClientOriginalName();
                    // $image->storeAs('uploaded',$original_name);

                    //   Image Intervention Package
                     $image = $request->image;
                     $img = Image::make($image)->resize(300, 200)->blur(10);
                     
                     $public_path = public_path();
                     $original_name = $image->getClientOriginalName();

                     $img->save( $public_path . '/' . $original_name );

                }

        }
}
