<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    public function send()
    {
         $client = new Client();
         $response = $client->request("POST","http://alerts.smstempo.com/api/v4/",[
             "form_params" => [
                "method" => "sms" ,
                "sender" => "MCXMNA" ,
                "to" => '9646848434' ,
                'message' => "Hi this is" . now()->toDateTimeString() ,
                "api_key" => "A936d9675fb0ec9f769f4c9385829f0b5"
                
             ]
         ]);
         $res =  json_decode($response->getBody()->getContents());

         if($res->status == "OK"){
             return "Sms Send Successfully";
         } 
         return "Something went wrong";

    }
}
