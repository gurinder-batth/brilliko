<?php

namespace App\Http\Controllers;

use App\ExamStudent;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function passStudent(Request $request)
    {
        $marks = ">=";
        if($request->fail){
              $marks = "<=";
        }
        $exams =  ExamStudent::whereExamId(1)
             ->join("exam_result","exam_students.id","=","exam_result.exam_student_id")
              ->where('exam_result.marks', $marks, 33)
                 ->get();

        return view("exams",['exams' => $exams]);
    }
}
