@extends('master')

@section('content')
<div class="container">

    @if(session()->has('unsuccess'))

    <div class="alert alert-danger">
        <strong> {{session()->get('unsuccess')}} </strong>
    </div>

    @endif

        <form action="{{route('user.login')}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
             <div class="form-group">
                <label for="">Email</label>
                  <input type="text" class="form-control" name="email">
             </div>
             <div class="form-group">
                <label for="">Password</label>
                  <input type="text" class="form-control" name="password">
             </div>
             <div class="form-group">
                <button class="btn btn-primary">Login</button>
             </div>
        </form>
        
        </div>
@endsection