<?php
namespace Library ;

class Session {
  
    public function create($name,$value)
    {
          $_SESSION[$name] = $value;
    }

    public function delete($name)
    {
        if(isset($_SESSION[$name])){
            unset($_SESSION[$name]);
        }
    }

    public function use($name)
    {
        if(isset($_SESSION[$name])){
            return $_SESSION[$name];
        }
        return null;
    }

    public function useOnce($name)
    {
        if(isset($_SESSION[$name])){
            $session =  $_SESSION[$name];
             unset($_SESSION[$name]);
             return $session;
        }
        return null;
    }

    public function has($name)
    {
        if(isset($_SESSION[$name])){
             return true;
        }    return false;
    }
    

}

