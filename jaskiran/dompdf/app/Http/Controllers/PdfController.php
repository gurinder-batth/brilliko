<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    public function index()
    {

        $data = [

            "user" => [
                "name" => "gurinder" ,
                "address" => "ABC"
            ]

        ];

        $pdf = PDF::loadView('invoice', [ "s" => $data ]);
        return $pdf->stream();
    }
}
