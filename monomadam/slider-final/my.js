class Slider {

    set defaultCssClasses(css_class){
          this.css_class = css_class;
    }

     constructor(c,gallary,id){
         this.css_class = null;
         this.c = c;
         this.lastImage = null;
         this.gallary = gallary;
         this.content = document.getElementById(id);
     }

     createDefault(){
          this.content.appendChild(this.createImage())
     }


     createImage(){
        //  this.content.innerHTML = "";
         let Image = document.createElement('IMG');
         Image.src = this.gallary[this.c];
         Image.classList = this.css_class;
     
         if(this.c == this.gallary.length - 1){
            this.c = 0;
        } else {
            this.c ++;
        }
         this.lastImage = Image;
         return Image;
     }

     moveRight(){
         
         this.lastImage.classList.add("move_right");
     }

}


let gallary =  [
    'bird1.jpg' ,
    'bird2.jpg' ,
    'bird3.jpg' ,
];
const slider = new Slider(0,gallary,'content');
slider.defaultCssClasses = 'slider-image';
slider.createDefault();

setTimeout( () => {
    slider.moveRight();
} , 3000 )

setInterval( () => {

    slider.createDefault();
    setTimeout( () => {
        slider.moveRight();
    } , 3000 )

} , 4500 )
