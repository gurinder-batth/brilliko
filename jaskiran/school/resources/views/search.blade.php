@extends('master')

@section('content')
<div class="container">

    <br>

    <div style="position:relative">
        <div class="form-group form-inline" style="poistion:absolute">
            <input type="text" class="form-control" id="search">
            <button class="btn btn-primary">Search</button>
        </div>
        <div style="poistion:absolute">
            <div class="form-group" id="result">
            </div>
        </div>

        {{-- <div class="card">
            <div class="card-body">
                <div class="form-group" id="result">
                </div>
            </div> --}}


        </div>
        @endsection

        @section('scripts')
        <script>
            $("#search").keyup(


                function () {

                    if (this.value.length > 1) {

                        $.get(

                            '{{route("students.search")}}' + '?str=' + this.value, //URL

                            function (response) {

                                if (response.length > 0) {
                                    var list = "";
                                    response.forEach((e) => {
                                        list += "<li>" + e.fname + "</li>";
                                    });

                                    result = "<ul>" + list + "</ul>";


                                    $("#result").html(result);
                                } else {

                                    result = "<ul> <li> No Result Found </li> </ul>";


                                    $("#result").html(result);

                                }
                            }

                        )


                    }

                }

            );

        </script>
        @endsection
