@extends('master')


@section('content')
    <div class="container">
        <form action="{{route('register.request')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="">Name</label>
                 <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="">EMail</label>
                 <input type="text" name="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Password</label>
                 <input type="text" name="password" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Profile Pic</label>
                 <input type="file" name="pic" class="fl">
            </div>
            <div class="form-group">
               <button class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection