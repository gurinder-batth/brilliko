<?php include_once 'functions/functions.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LineBreak</title>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/custom.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=7r01uh98c8dxn6alkylj4f5k4vdb0puxvtyksitbbzoauxpv"></script> 
</head>
<body>

<?php include_once 'nav.php'; ?>


