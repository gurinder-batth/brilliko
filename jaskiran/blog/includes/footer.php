<section class="hero is-dark">
    <div class="hero-body">
        <div class="container">
            <div class="columns">

                <div class="column is-3">
                    <div>Top Categories</div>
                    <div style="margin-left:12px">
                        <?php $categories = categories(); ?>
                        <?php while($category =  mysqli_fetch_assoc($categories)) { ?>
                        <a class="quicksand">
                            <li>
                                <?= $category['name'] ?>
                            </li>
                        </a>
                        <?php } ?>
                    </div>
                </div>

                <div class="column is-3">
                    <div>Top Authors</div>
                    <div style="margin-left:12px">
                        <a class="quicksand">
                            <li>Gurinder Batth</li>
                            <li>Seth Rollins</li>
                            <li>Talyor</li>
                            <li>Shinchan</li>
                            <li>Tom</li>
                        </a>
                    </div>
                </div>

                <div class="column is-3">
                    <div>Top Recent Posts</div>
                    <div style="margin-left:12px">
                        <?php $posts = recent_posts(); ?>
                        <?php while($post =  mysqli_fetch_assoc($posts)) { ?>
                        <a class="quicksand">
                            <li>
                                <?= substr($post['title'],0,18) ?>...</li>
                        </a>
                        <?php } ?>
                    </div>
                </div>

                <div class="column is-3">
                    <div>Recent Comments</div>
                    <div style="margin-left:12px">
                        <?php $posts = recent_posts(); ?>
                        <?php while($post =  mysqli_fetch_assoc($posts)) { ?>
                        <a class="quicksand">
                            <li>
                                <?= substr($post['title'],0,18) ?>...</li>
                        </a>
                        <?php } ?>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<footer class="footer" style="padding: 3rem 6rem;">
    <div class="content has-text-centered">
        <p>
            <strong> <strike class=""> <span class="quicksand "><span class="is-size-5">Line</span><span class="is-size-4 has-text-weight-bold has-text-info">Break</span></span>
                </strike> </strong> by <a href="https://jgthms.com">Gurinder Batth</a>. The source code is licensed
            <a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
            is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY NC SA 4.0</a>.
            <br>
            ©
            <?= date('Y'); ?>
        </p>
    </div>
</footer>

<script src="public/js/my.js"></script>
<script>
  tinymce.init({
    selector: '#mytextarea'
  });
  </script>
</body>
</html>