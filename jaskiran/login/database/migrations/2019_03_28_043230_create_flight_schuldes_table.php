<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightSchuldesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight_schuldes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('departure');
            $table->dateTime('departure_at');
            $table->string('arrive');
            $table->dateTime('arrive_at');
            $table->unsignedBigInteger('flight_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight_schuldes');
    }
}
