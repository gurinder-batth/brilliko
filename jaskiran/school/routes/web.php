<?php


Route::view('/','register');
Route::view('login','login')->name('user.login_form');
Route::post('register','UserController@register')->name('user.register');
Route::post('login','UserController@login')->name('user.login');

Route::view('profile','profile')->name('user.profile');

Route::get('logout','UserController@logout')->name('user.logout');



Route::namespace('Student')->group(function(){

    Route::prefix('students')->group(function(){

         Route::get('list','StudentController@list')->name('students.list');
         Route::get('add','StudentController@add')->name('students.add');


    });

});


Route::get('search','Search@main')->name('students.search');
Route::view('search_me','search')->name('students.search.view');

// http://jaskiran.brilliko/school/students/list