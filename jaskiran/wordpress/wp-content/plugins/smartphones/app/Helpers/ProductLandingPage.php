<?php
namespace App\Helpers;

use GuzzleHttp\Client;
use Library\DefaultConfiguration;
use Library\Session;
use Library\Request;
use DOMDocument;

class ProductLandingPage extends DefaultConfiguration{

    public $products;

    public function getData()
    {
            
        
           $proRegex = '|<a\shref="(.*)"><img|iU';
           preg_match_all($proRegex,$this->product,$pUrl);

           $singleProductHTML = $this->client->request('GET',$this->url.$pUrl[1][0]);
           $singleProductHTML = $singleProductHTML->getBody()->getContents();

  
           $regexSingle = '|<div class="main main-review right l-box col">([\s\S]*)<script language="JavaScript">|';
           preg_match_all($regexSingle,$singleProductHTML,$productHeader);

           $regexSingleTable = '|<div id="specs-list">([\s\S]*)<\/table>|';
           preg_match_all($regexSingleTable,$singleProductHTML,$productTable);


           $this->bodyUpper =  $productHeader[1][0];
           $this->bodyBelow =  $productTable[1][0] . "</table>" ;
           $this->bodyBelow = preg_replace('<a href="(.*)">', 'a', $this->bodyBelow); 

           

    }

    public function title($key)
    {
          $titleRegex = '|<a href=".*?<strong>(.*?)<\/strong>|';
          preg_match_all($titleRegex,$this->products[1][$key],$title);
          $this->title = $title[1][0];
    }

    public function insertPost()
    {

       $id =  wp_insert_post( [
            "post_name" => $this->title ,
            "post_title" => $this->title ,
            "post_content" => mb_convert_encoding($this->bodyBelow,"UTF-8") ,
            "post_category" => [$this->request->category] ,
            'post_status' => 'publish' ,
            'post_type' => 'post'
         ],true);

    }

    public function remove_4_byte($string) {
        $char_array = preg_split('/(?<!^)(?!$)/u', $string );
        for($x=0;$x<sizeof($char_array);$x++) {
            if(strlen($char_array[$x])>3) {
                $char_array[$x] = "";
            }
        }
        return implode($char_array, "");
    }

}