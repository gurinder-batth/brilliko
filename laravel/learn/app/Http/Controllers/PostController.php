<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function addForm()
    {
        return view('post_add');
    }

    public function add(Request $request)
    {
        Post::create($request->all());
        session()->flash("success","Post successfully created");
        // return redirect()->back();
        return redirect()->route('post.recent');
    }

    public function get()
    {
         $posts = Post::latest()->paginate(2);
         return view("posts")->withPosts($posts);
    }

    public function edit(Request $request)
    {
        $post = Post::find($request->id);
         return view("edit_form")->withPost($post);
    }

    public function delete(Request $request)
    {
         $post = Post::find($request->id);
         $post->delete();
         return redirect()->back();
    }


    public function update(Request $request)
    {
        $request->validate([
            "title" => "required|min:3|max:255" ,
            "description" => "required|min:3|max:255"
        ]);

        

        $post = Post::find($request->id);
        $post->title = $request->title;
        $post->description = $request->description;
        $post->save();
        session()->flash("success","Changes successfully updated");
        return redirect()->back();
    }


    
}
