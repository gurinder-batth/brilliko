<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
    public function loginForm()
    {
       return view("login");
    }
    public function registerForm()
    {
        return view("register");
    }

    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email , 'password' => $request->password])){
            
             return redirect()->route("post.recent");

        }

        return redirect()->back();
    }


    public function register(Request $request)
    {
       User::create([
           'name' => $request->name ,
           'email' => $request->email ,
           'password' => bcrypt($request->password) ,
           'pic' => $request->pic->store("pics") ,
       ]);

       return redirect()->back();
    }

    public function logout()
    {
       Auth::logout();
       return redirect()->route("login");
    }
}
