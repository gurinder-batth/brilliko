<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>My First Bootstrap Page</h1>
  <p>Resize this responsive page to see the effect!</p> 
</div>

<div class="container">
  <div class="row">
    <div class="col-md-4">
      <form action="{{route('students.results')}}" name="result_form" method="get">
        <select name="res" id="" class="form-control" onchange="document.result_form.submit()">
              <option value="all" >All</option>
              <option value=">=" @if(request()->get("res") == ">=") selected @endif>Pass</option>
              <option value="<" @if(request()->get("res") == "<") selected @endif>Fail</option>
        </select>
      </form>
    </div>
  </div>
</div>
  
<div class="container">
  <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr>
                    <th>Sr</th>
                    <th>Name</th>
                    <th>Marks</th>
                </tr>
                @foreach ($exams as $i =>$exam)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{$exam->student->name}}</td>
                        <td>{{$exam->marks}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
  </div>
</div>

</body>
</html>
