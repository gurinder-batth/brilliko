<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // factory(User::class, 1)->create()->each(function($user){
            
        //     $role = new Role();
        //     $role->user_id = $user->id;
        //     $role->premission = json_encode($role->roles());
        //     $role->save();

        // });
        factory(User::class, 1)->create()->each(function($user){
            
            $role = new Role();
            $role->user_id = $user->id;
            $role->premission = json_encode($role->rolesHR());
            $role->save();

        });
    }
}
