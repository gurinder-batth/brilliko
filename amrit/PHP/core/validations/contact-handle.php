<?php
session_start();
// $_POST


$name =  $_POST['name'];
$city =  $_POST['city'];
$mobile =  $_POST['mobile'];
$address =  $_POST['address'];


// 1) Name Required ->
// Logic -> $name string length  == 0 
// Amrit 5

if(  strlen($name) == 0 ){
    $_SESSION['error'] = "Name should be required";
    header("Location:contact2.php");
    exit;
}
if(  strlen($city) == 0 ){
    $_SESSION['error'] = "City should be required";
    header("Location:contact2.php");
    exit;
}

//  IF name 3 ,    10

// Logic -> strlen($name) < 3 || strlen($name) > 10

if(strlen($name) < 3 || strlen($name) > 10){
    $_SESSION['error'] = "Name should be greater than 3chars and less than 10 chars :)";
    header("Location:contact2.php");
    exit;
}

if(is_numeric($mobile) == false){
    $_SESSION['error'] = "Mobile Number only  Contains numeric chars";
    header("Location:contact2.php");
    exit;
}