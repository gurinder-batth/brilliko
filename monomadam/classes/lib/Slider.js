class Slider extends Helper{

     constructor(id){
        super();
        this.currentImg  =  0;
        this.slider = this.id(id)
        this.images = [];
        this.img = null;
     }

     setImages(images){
         this.images = images
     }

     createSlide(){
              let div = document.createElement("DIV")
              div.classList = "slide"
              let img = document.createElement("IMG")
              img.src = this.images[this.currentImg];
              img.onerror = () => {
                img.src = "https://www.unesale.com/ProductImages/Large/notfound.png"
              }
              this.img = img;
              div.appendChild(img)
              this.slider.appendChild(div);

     }

     run(){
                 this.createSlide();
                 setInterval( () => {
                       
                        this.currentImg++;
                        console.log()
                        this.img.src = this.images[this.currentImg];
                     
      
                 } ,3000 )
     }
     

}


