<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
  public function register(Request $request)
  {

        $request->validate([
             'name' => 'required' ,
             'email' => 'required|email' ,
             'password' => 'required|min:4|max:12'
        ]);


         User::insert([
              'name' => $request->name ,
              'email' => $request->email ,
              'password' => bcrypt($request->password)
         ]);

         session()->flash('success','You are successfully Register Please Login');
         return redirect()->back();

  }

   public function login(Request $request)
   {

       $status =  Auth::attempt( [ 'email' => $request->email , 'password' => $request->password ] );

       if($status){
         return   redirect()->route('user.profile');
       }

       return   redirect()->back();

   }

   public function logout()
   {
       Auth::logout();
       return redirect()->route('user.login_form');
   }

}
