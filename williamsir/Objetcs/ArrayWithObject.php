<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Object With Array</title>
</head>
<body>
    

    <script>

    // Array
            var country  = [

            // Object 
             [
                // Property:Value{String}
                 {name:"Delhi"} ,
                 {name:"Punjab"} ,
                 name:"ETC" ,

             ] ,

            //  Object
             {
                 name:"Sangrur" ,
                 name:"Ludhiana",
                 name:"ETC"
             }


            ];

            console.log(country[0])
            // for(var i = 0; i< country[0].length ; i++){
            //      country[0]
            // }
    </script>

</body>
</html>