<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    protected $fillable = ['fname','lname','mobile','active'];

    protected $hidden = ['mobile'];

}
