@extends("admin.master")
@section('content')
<div class="container">
        @include('admin.includes.error_message')    
    <form id="stuentForm" method="POST" action="{{route('students.add_post')}}">
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="brilliko_text label">First Name</label>
                    <input type="text" name="first_name" id="" class="form-control" required minlength="4" maxlength="15">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="brilliko_text label">Last Name</label>
                    <input type="text" name="last_name" id="" class="form-control" required minlength="4" maxlength="15">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">Qualification</label>
                    <select name="qualification" class="form-control" required>
                        <option value="">Select Qualification</option>
                        <option>10th</option>
                        <option>12th</option>
                        <option>Gradudate</option>
                        <option>Post Gradudate</option>
                    </select>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">Email</label>
                    <input type="email" class="form-control" name="email">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">Mobile</label>
                    <input type="tel" class="form-control" name="mobile" required minlength="10" maxlength="10">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">Select Gender:</label>
                    <div class="select is-fullwidth is-rounded">
                        <select name="gender" class="form-control" required="">
                            <option value="">Select</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">DOB:</label>
                    <div class="control">
                        <input type="date" class="form-control" name="dob" id="" required>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">Marital Status:</label>
                    <select name="martial_status" class="form-control" required>
                        <option value="">Marital Status</option>
                        <option>Single</option>
                        <option>Married</option>
                    </select>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">City/State</label>
                    <input type="tel" class="form-control" name="city" required>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">Working Type</label>
                    <input type="text" name="working_type" id="" class="form-control" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">Father Name</label>
                    <input type="text" name="father_name" id="" class="form-control" required minlength="4" maxlength="15">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">Occupation</label>
                    <input type="text" name="occupation" id="" class="form-control" required>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label brilliko_text">Mobile</label>
                    <input type="tel" name="father_mobile" id="" class="form-control" required minlength="10" maxlength="10">
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group">
                    <label class="label brilliko_text">Complete Address</label>
                    <textarea name="address" class="form-control" required></textarea>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-4">
                <label class="brilliko_text">Course Duration</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="course_duration" id="course_duration" required>
                </div>
            </div>

            <div class="col-lg-4">
                <label class="brilliko_text">Course Name</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="course_name" id="" required>
                </div>
            </div>

            <div class="col-lg-4">
                <label class="brilliko_text">Joining Date</label>
                <div class="form-group">
                    <input type="date" class="form-control" value="" name="joining_date" id="joining_date" required>
                </div>
            </div>

            <div class="col-lg-4">
                <label class="brilliko_text">Batch Time From</label>
                <div class="form-group">
                    <input type="time" class="form-control" name="batch_from" id="" required>
                </div>
            </div>

            <div class="col-lg-4">
                <label class="brilliko_text">Batch Time To</label>
                <div class="form-group">
                    <input type="time" class="form-control" name="batch_to" id="" required>
                </div>
            </div>

            <div class="col-lg-4">
                <label class="brilliko_text">Completion Date</label>
                <div class="form-group">
                    <input type="date" disabled id="completion_date" class="form-control" id=""
                        required>
                    <input type="hidden"  id="completion_date_hidden" class="form-control" name="completion_date" id=""
                        required>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-lg-4">
                <label class="brilliko_text">Total Fees</label>
                <div class="form-group">
                    <input type="number" name="total_fees" class="form-control" id="total_fees" minlength="1" required>
                </div>
            </div>

            <div class="col-lg-4">
                <label class="brilliko_text">Advance Amount</label>
                <div class="form-group">
                    <input type="number" name="advance" class="form-control" id="advance_amount" minlength="1"
                        required>
                </div>
            </div>

            <div class="col-lg-4">
                <label class="brilliko_text">Installment Months</label>
                <div class="form-group">
                    <input type="number" name="months" class="form-control" id="installment_months"
                        minlength="1" required>
                </div>
            </div>

            <div class="col-lg-4">
                <label class="brilliko_text">Payment Type</label>
                <div class="form-group">
                    <textarea name="payment_type" class="form-control" id="" minlength="1" required></textarea>
                </div>
            </div>

            {{-- Hidden Fields --}}
              <input type="hidden" name="due_date" id="due_date">
              <input type="hidden" name="last" id="last" value="0">
              <input type="hidden" name="amount" id="amount" >
            {{-- End Hidden Field --}}

            <div class="col-lg-12">
                <table class="table table-bordered">

                    <tr class="brilliko_text text-muted">
                        <th>Total</th>
                        <th>Advance</th>
                        <th>Next Installment</th>
                        <th>First <span id="number_month"></span> Months</th>
                        <th>Last Month</th>
                    </tr>

                    <tr class="brilliko_text ">
                        <th><span id="showTotal"></span></th>
                        <th><span id="showAdvance"></span></th>
                        <th><span id="showNext"></span></th>
                        <th><span id="showMonthlyAmount"></span></th>
                        <th><span id="showLast"></span></th>
                    </tr>

                </table>
            </div>





        </div>

        <div class="row">
            <div class="col-lg-6">
                <button class="btn btn-primary">Create New Student</button>
            </div>
        </div>

    </form>
</div>
<br>
@section('scripts')
<script>
    jQuery(document).ready(function () {

    
        let date = new Date().toISOString().split('T');
        jQuery('#joining_date').val(date[0]);
        next_due_date();

        jQuery("#stuentForm").validate();

        jQuery('#course_duration').keyup(update_completion);
        jQuery('#joining_date').keyup(update_completion);
        jQuery('#joining_date').keyup(next_due_date);
        jQuery('#total_fees').keyup(update_installments);
        jQuery('#advance_amount').keyup(update_installments);
        jQuery('#installment_months').keyup(update_installments);

        function update_installments() {
            var total_fees = jQuery('#total_fees').val();
            var advance = jQuery('#advance_amount').val();
            var months = jQuery('#installment_months').val();
            var m_installment = '';
            if (total_fees == '' || advance == '' || months == '') {
                return false;
            }
            // Get the Left Amount
            let left_amount = total_fees - advance;
            console.log(left_amount)
            // Get the Paid Amount of Every Installment
            let per_installements = left_amount / months;
            // The Divide the Amount With 1000 
            let x = per_installements / 1000;
            // Round of The X and Save it into y
            let y = Math.round(x);
            // If our Round of Amount (y) is greater than x amount Then Subtract 1 from it
            if (y > x) {
                y--;
            }
            // Now Multiple y with 1000 
            m_installment = y * 1000;
            // Now Go For Last Installment
            let diff = (x * 1000) - (y * 1000);
            // now diff multiple with months  to get Round of Amount
            let round = Math.round((diff * months) / 1000);
            last = round * 1000;
            cal = true;

            updateShowTable(months,last,m_installment);
            //console.log("Installment Per Month " + m_installment)
            //console.log("Last Installmet " + last)
        }

        function update_completion() {
            if (jQuery('#course_duration').val() != '') {
                var completion_date = new Date(jQuery('#joining_date').val())
                let duration = parseInt(jQuery('#course_duration').val());
                completion_date.setMonth(completion_date.getMonth() + duration);
                jQuery('#completion_date').val(completion_date.toISOString().split('T')[0])
                jQuery('#completion_date_hidden').val(completion_date.toISOString().split('T')[0])
            }
        }

        function next_due_date() {
            var due_date = new Date(jQuery('#joining_date').val())
            let day = due_date.getDate();
            due_date.setMonth(due_date.getMonth() + 1);
            if (day <= 15) {
                due_date.setDate(7);
            } else {
                due_date.setDate(22);
            }
            jQuery('#showNext').html(due_date.getDate()+"-"+getmonths(due_date.getMonth())+"-"+due_date.getFullYear());
            due_date = due_date.toISOString().split('T')[0];
            jQuery('#due_date').val(due_date);
        }

        function updateShowTable(n,last,amount) {
            jQuery("#showTotal").html(jQuery("#total_fees").val());
            jQuery("#showAdvance").html(jQuery('#advance_amount').val());
            //jQuery("#showMonths").html(n);
            if(last == 0){
                jQuery("#showLast").html("---");
            }else{
                jQuery("#showLast").html(""+last+"");
                jQuery("#last").val(last);
            }
            jQuery("#number_month").html(n);
            jQuery("#showMonthlyAmount").html(amount);
            jQuery("#amount").val(amount);
            
        }

        function getmonths(i){
         let months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
         return months[i];
       }


    })
</script>
@endsection
@endsection