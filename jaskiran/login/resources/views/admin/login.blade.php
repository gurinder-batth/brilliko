@extends('master')

@section('content')


@if($errors->any())
       <div class="alert alert-danger">
             {{$errors->first()}}
       </div>
@endif

@if(session()->has('unsuccess'))
<div class="alert alert-danger">
        {{session()->get('unsuccess')}}
  </div>
@endif




@if(session()->has('success'))
<div class="alert alert-success">
        {{session()->get('success')}}
  </div>
@endif

<h1>Admin Login</h1>
 <form action="{{route('admin.login')}}" method="post">
    {{ csrf_field() }}

    <div class="form-group">
            <label for="">Mobile</label>
            <input class="form-control" name="mobile">
    </div>

    <div class="form-group">
            <label for="">Password</label>
            <input class="form-control" name="password">
    </div>



    <div class="form-group">
          <button class="btn btn-primary">Login</button>
    </div>

    <div class="form-group">
          <a href="{{route('otp.login')}}" class="btn btn-primary">Login with OTP(One Time Password)</a>
    </div>

    <div class="form-group">
          <a  href="{{route('password.forget.form')}}">Forget Password</a>
    </div>

    

    <h1>Don't have account yet ? <a href="{{route('admin.register')}}"> Register</a></h1>
</form>

@endsection