<div class="section">
    <div class="container">
       
       <?php
         
         if($session->has('success')){
             echo $session->useOnce('success');
         }

        ?>

        <form action="" method="POST">
            <!-- URL FIELD -->
            <div class="field column">
                <label class="label">URL</label>
                <div class="control">
                    <input class="input is-dark" type="text" name="url" placeholder="Enter URL From GSMArena.com" required>
                </div>
            </div>

  
            <!-- Select Category -->
            <div class="field  column ">
                <label class="label">Category</label>
                <div class="control">
                    <div class="select is-dark is-fullwidth ">
                        <select name="category" class="input" required>
                            <option>Select dropdown</option>
                            <?php foreach($categories as $category) { ?>
                            <option value="<?= $category->term_id; ?>">
                                <?= $category->name ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <!-- <button id="upload-btn">Upload</button> -->

            <input type="hidden" name="method" value="<?= $method ?>">
            <input type="hidden" name="class_name" value="Scrap">
            <input type="hidden" name="class_method" value="fetchData">

            <div class="field is-grouped  column is-6 inline">
                <div class="control">
                    <button class="button is-link">Submit</button>
                </div>
                <div class="control">
                    <button class="button is-text">Cancel</button>
                </div>
            </div>

        </form>
    </div>
</div>
