<?php

namespace App;

use App\ExamResult;
use Illuminate\Database\Eloquent\Model;

class ExamStudent extends Model
{
    protected $table = "exam_students";

    public function student()
    {
        return $this->hasOne(Student::class,"id","student_id");
    }    

    public function result()
    {
        return $this->hasOne(ExamResult::class,"exam_student_id","id");
    }
}
