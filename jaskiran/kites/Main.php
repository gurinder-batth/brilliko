<?php include_once 'Display.php'; ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Kites</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
  <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>

<body>
  <section class="section">
    <div class="container">
      <?php 
        $display = new Display();
        $kites = $display->get();
    ?>


      <?php foreach($kites as $kite) {  ?>
        <div class="notification is-dark">
              <button class="delete"></button>
              <h1>  <?= $kite->name ?></h1>
     </div>
      <?php } ?>

      <nav class="pagination" role="navigation" aria-label="pagination">
       <?php if($display->pre != null) { ?>
        <a class="pagination-previous" href="Main.php?page=<?= $display->pre ?>">Previous</a>
       <?php } ?>
        <a class="pagination-next" href="Main.php?page=<?= $display->next ?>">Next page</a>
   </nav>




    </div>
  </section>
</body>

</html>