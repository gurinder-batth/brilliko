<?php

include_once 'SmsLogger.php';
include_once 'EmailLogger.php';
include_once 'LoggerInterface.php';

class Order{

    public function placed(LoggerInterface $logger)
    {
      $logger->send('order placed');
    }

    public function cancel(LoggerInterface $logger)
    {
        $logger->send('order cancel');
    }

}

$logger = new EmailLogger();
$order = new Order();
$order->placed($logger);