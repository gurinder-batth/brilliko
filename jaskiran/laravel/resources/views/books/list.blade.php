@extends('books.master')

@section("content")


@if(session()->has('success'))

<div class="notification is-success">
    <strong> {{session()->get('success')}} </strong>
</div>

@endif


<a href="{{route('books.insert_form')}}" class="button is-link">Insert New Book</a> 
<br> <br>

<table class="table is-fullwidth is-bordered">

     <tr>
         <th>Id</th>
         <th>Book</th>
         <th>Author</th>
         <th>Price</th>
         <th>Edit</th>
         <th>Delete</th>
     </tr>

     @foreach ($books as $book)
      
         <tr>

               <td>{{$book->id}}</td>
               <td>{{$book->name}}</td>
               <td>{{$book->author}}</td>
               <td>{{$book->price}}</td>
              <td> <a href="{{route('books.edit' , [ 'id' => $book->id ]  )}}"> Edit </a> </td>
              <td> <a href="#" class="button is-danger" onclick="deleteBook('{{route('books.delete' , [ 'id' => $book->id ]  )}}')">Delete</a> </td>

         </tr>
     
     @endforeach

</table>

<div class="modal" id="mymodel">
    <div class="modal-background"></div>
    <div class="modal-content">

        <div class="notification">

             Are you Really want to delete? <br>
             <a href="" id="okay_btn"><button class="button is-link">Okay</button></a>
             <button class="button is-danger"  onclick="hideModel()">Discard</button>

        </div>


    </div>
    <button class="modal-close is-large" aria-label="close" onclick="hideModel()"></button>
  </div>



@endsection


@section('scripts')
<script>

var model = document.getElementById('mymodel');
var okay = document.getElementById('okay_btn');
function deleteBook(link){
okay.href = link;
 model.classList.add("is-active");
}

function hideModel(){
 model.classList.remove("is-active");
}

</script>
@endsection