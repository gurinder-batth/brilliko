@extends('master')

@section('content')


@if($errors->any())
       <div class="alert alert-danger">
             {{$errors->first()}}
       </div>
@endif

@if(session()->has('unsuccess'))
<div class="alert alert-danger">
        {{session()->get('unsuccess')}}
  </div>
@endif




@if(session()->has('success'))
<div class="alert alert-success">
        {{session()->get('success')}}
  </div>
@endif


<form action="{{route('otp.verification.request')}}" method="post">
    {{ csrf_field() }}

    <div class="form-group">
            <label for="">OTP</label>
            <input class="form-control" name="otp">
    </div>

    <div class="form-group">
     <button class="btn btn-primary">Login</button>
</div>


</form>

@endsection