<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
class BookController extends Controller
{

    public function main(Request $request)
    {
                $books =  Book::get();
                return view('books.list')->withBooks($books);
    }


    public function edit(Request $request)
    { 
         $book = Book::find($request->id);
         return view('books.edit')->withBook($book);
    }

    public function update(Request $request)
    { 
         $book = Book::find($request->id);
         $book->author =  $request->author;
         $book->name = $request->name;
         $book->price = $request->price;
         $book->save();

         session()->flash("success",$book->name . " is successfully updated!");
         return redirect()->route('books.list');
    }


    public function delete(Request $request)
    { 
         $book = Book::find($request->id);
         $book->delete();
         return redirect()->back();
    }


    public function insertForm()
    {
        return view('books.insert');
    }
    

    public function insert(Request $request)
    {

         Book::insert([

          'name' => $request->name ,
          'author' => $request->author ,
          'price' => $request->price 

         ]);
         session()->flash("success","Book is successfully inserted!");
        return redirect()->route('books.list');


    }



    


}
