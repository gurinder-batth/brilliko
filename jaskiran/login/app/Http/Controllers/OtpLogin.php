<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Mail\OTP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OtpLogin extends Controller
{

     public function loginForm()
     {
         return view("admin.otp_login");
     }

     public function otpForm()
     {
        if(! session()->has("otp") ){
            return redirect()->route("otp.login");
       }

         return view("admin.otp_verification");
     }

     public function otpSend(Request $request)
     {
        $admin =  Admin::whereEmail($request->email)->first();

        if($admin == null){
            session()->flash("unsuccess","Oops! We don't find your account on our server");
            return redirect()->back();
        }

         $otp  =  rand(1000,9999);
         session()->put("otp",$otp);
         session()->put("user",$admin);
         Mail::to($request->email)->send(new OTP($otp));
         session()->flash("success","Please Check Your Registered Email Id for OTP");
         return redirect()->route("otp.verification");
         
     }
     public function otpVerfication(Request $request)
     {

        
        //  Checkpoint
        if(! session()->has("otp") ){
            return redirect()->route("otp.login");
        }


        if(session()->has("attempt")){
                $attempt = session()->get("attempt");
                $attempt++;

                if($attempt == 4){
                    session()->forget("user");
                    session()->forget("attempt");
                    session()->forget("otp");
                    return redirect()->route('admin.dashboard');
                }
                
                session()->put("attempt",$attempt);
        } else {
            session()->put("attempt",1);
        }
        

        $otp = session()->get("otp");
  
        if($request->otp ==  $otp){
           //  Login
            $admin = session()->get("user");
            session()->forget("user");
            session()->forget("otp");
            Auth::guard('admin')->loginUsingId($admin->id);
            return redirect()->route('admin.dashboard');
            
        }

        session()->flash("unsuccess","Please Enter Valid OTP");
        return redirect()->back();
     }

}
