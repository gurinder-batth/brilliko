@extends('master')

@section('content')
<h1>Users</h1>
 <ul>
     @foreach ($users as $user)
         <li>{{$user->name}}</li>
     @endforeach
 </ul>
 <h1>Admins</h1>
 <ul>
     @foreach ($admins as $admin)
         <li>{{$admin->mobile}}</li>
     @endforeach
 </ul>
@endsection