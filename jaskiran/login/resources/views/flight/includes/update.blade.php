<!-- Modal -->
<div id="updateModel" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body">
                    <form action="{{route('flight.schulde.update')}}" name="update_schulde_form" method="post">
                            {{ csrf_field() }}
                              {{method_field('PATCH')}}
                            <div class="form-group">
                                    <label for="">Departure From</label>
                                    <input class="form-control" name="departure">
                            </div>
                
                                <div class="form-group">
                                        <label for="">Departure At Date</label>
                                        <input  type="date" class="form-control" name="departure_at_date">
                              </div>
                                <div class="form-group">
                                        <label for="">Departure At Time</label>
                                        <input  type="time" class="form-control" name="departure_at_time">
                              </div>
    
                            <div class="form-group">
                                    <label for="">Arrive </label>
                                    <input class="form-control" name="arrive">
                            </div>
                
                                <div class="form-group">
                                        <label for="">Arrive At Date</label>
                                        <input  type="date" class="form-control" name="arrive_at_date">
                              </div>
    
                                <div class="form-group">
                                        <label for="">Arrive At Time</label>
                                        <input  type="time" class="form-control" name="arrive_at_time">
                              </div>


                                      <input type="hidden" name="flight_id" value={{$flight->id}}>

                                        <input  type="hidden" class="form-control" name="id">
               
                          
    
                                <div class="form-group">
                                        <button class="btn btn-primary">Update Schulde</button>
                              </div>
                      </form>
                      
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
      
        </div>
</div>


<script>
  function updateSchuldeForm(schulde_model) {
       
       let schulde = JSON.parse(schulde_model)
       let updateForm = document.update_schulde_form;

       updateForm.departure.value = schulde.departure;
       updateForm.departure_at_date.value = schulde.departure_at.split(" ")[0];
       updateForm.departure_at_time.value = schulde.departure_at.split(" ")[1];


       updateForm.arrive.value = schulde.arrive;
       updateForm.arrive_at_date.value = schulde.arrive_at.split(" ")[0];
       updateForm.arrive_at_time.value = schulde.arrive_at.split(" ")[1];

       updateForm.id.value = schulde.id;
       return ;
      


    }
</script>