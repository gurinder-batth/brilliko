@extends('master')


@section('content')
    <div class="container">
        <form action="{{route('login.request')}}" method="post" >
            {{ csrf_field() }}

            <div class="form-group">
                <label for="">EMail</label>
                 <input type="text" name="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Password</label>
                 <input type="text" name="password" class="form-control">
            </div>

            <div class="form-group">
               <button class="btn btn-primary">Login</button>
            </div>
        </form>
    </div>
@endsection