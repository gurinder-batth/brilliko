<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
         public function login(Request $request)
         {

            $request->validate([
                'email' => 'required|email' ,
                'password' => 'required|min:3|max:15' 
              ]);

              $status = Auth::attempt( [ 'email' => $request->email , 'password' => $request->password ] );

              if($status){
                  return redirect()->route('welcome');
              }

              session()->flash('unsuccess','Invalid username and password'); // Use only Once After that destory
            //   session()->put('unsuccess','Invalid username and password'); //Use Unlimted Times
              return redirect()->back();


         }
         public function register(Request $request)
         {

               $request->validate([
                
                 'name' => 'required|min:3|max:15' ,
                 'email' => 'required|email|unique:users,email' ,
                 'password' => 'required|min:3|max:15'
                         
               ]);


            //  Mass Assginment Error
                User::create([

                     'name' => $request->name ,
                     'email' => $request->email ,
                     'password' => bcrypt($request->password)

                ]);
         }

         public function logout()
         {
              Auth::logout();
              return redirect()->back();
         }
}
