<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function list()
    {
        // Fetch the orders from orders table
        $orders = Order::latest()->get();
        // return response in json
        return response()->json($orders);
    }


    public function add(Request $request)
    {
        // normal text
       $orders = json_decode($request->orders);
       foreach($orders as $order){
           Order::create([
              'product_name' => $order->product_name ,
              'product_price' => $order->product_price ,
              'product_id' => $order->product_id ,
           ]);

           return response(['status'  => "Ok"]);
       }
    }
}
