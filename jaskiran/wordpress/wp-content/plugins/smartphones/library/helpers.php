<?php
use  Library\DefaultConfiguration;


function view($view_name,$data = []){
    $conf = new DefaultConfiguration();
    extract($data);
    include_once $conf->plugin_path . 'views/' . $view_name . '.php';
}

function dd($var){
    dump($var);
    exit;
}