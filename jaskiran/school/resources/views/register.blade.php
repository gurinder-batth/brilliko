@extends('master')

@section('content')
<div class="container">

    @if(session()->has('success'))

    <div class="alert alert-info">
        <strong> {{session()->get('success')}} </strong>
    </div>

    @endif

        <form action="{{route('user.register')}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
                <label for="">Name</label>
                  <input type="text" class="form-control" name="name">
             </div>
             <div class="form-group">
                <label for="">Email</label>
                  <input type="text" class="form-control" name="email">
             </div>
             <div class="form-group">
                <label for="">Password</label>
                  <input type="text" class="form-control" name="password">
             </div>
             <div class="form-group">
                <button class="btn btn-primary">Register</button>
             </div>
        </form>
        
        </div>
@endsection