<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            {{-- Meri Bili :) --}}

            <!-- Modal body -->
            <div class="modal-body" v-if='data != null'>
                
                <div>
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control" v-model="data.name">
                    </div>
                    <div class="form-group">
                        <label for="">Phone</label>
                        <input type="text" class="form-control" v-model="data.phone">
                    </div>
                    <div class="form-group">
                        <label for="">Course</label>
                        <select name="course_id"  class="form-control" v-model="data.course_id">
                                @foreach ($courses as $course)
                                    <option value={{$course->id}} 
                                    @if(request()->get('course_id') == $course->id)
                                        selected="selected"
                                    @endif
                                    >
                                    {{$course->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Batch</label>
                        <select name="batch_id"  class="form-control" v-model="data.batch_id" >
                                @foreach ($batchs as $batch)
                                <option value={{$batch->id}} 
                                 @if(request()->get('batch_id') == $batch->id)
                                     selected="selected"
                                 @endif
                                 >
                                 {{$batch->name}}</option>
                               @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Marks</label>
                        <input type="text" class="form-control" v-model="data.marks" >
                    </div>
                    <div class="form-group">
                        <label for=""></label>
                        <button class="btn btn-primary" @click="update">Update</button>
                    </div>
                </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
