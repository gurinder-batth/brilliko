@extends('master')

@section('content')


@if($errors->any())
       <div class="alert alert-danger">
             {{$errors->first()}}
       </div>
@endif

@if(session()->has('unsuccess'))
<div class="alert alert-danger">
        {{session()->get('unsuccess')}}
  </div>
@endif


 <form action="{{route('login.post')}}" method="post">
    {{ csrf_field() }}

    <div class="form-group">
            <label for="">Email</label>
            <input class="form-control" name="email">
    </div>

    <div class="form-group">
            <label for="">Password</label>
            <input class="form-control" name="password">
    </div>

    <div class="form-group">
          <button class="btn btn-primary">Login</button>
    </div>

</form>

@endsection