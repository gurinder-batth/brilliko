<?php

namespace App\Http\Controllers;

use auth;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        
        // $token = JWTAuth::attempt(["email" => $request->email,'password' => $request->password]);
        $token = JWTAuth::attempt($credentials);
              
         if($token == null){

            return response()->json(['status' => false]);
         }

         return response()->json(['status' => true , 'token' => $token]);

    }

     public function signup(Request $request)
     {
        $user = User::create([
                'name' => $request->name ,
                'email' => $request->email ,
                'password' => bcrypt($request->password) ,
            ]);
          $token = JWTAuth::fromUser($user);
          return response()->json($token);
     }

     public function dashboard(Request $request)
     {
          $user = JWTAuth::parseToken()->authenticate();
          return response()->json($user);
     }
     
}
