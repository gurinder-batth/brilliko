<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <style>
     .box{
         background:red;
         height:100px;
         width:100px;
         position:absolute;
         top:0;
         left:0;
     }
    </style>
</head>

<body>

<div class="box"></div>


 <button class="btn btn-primary" id="btn" style="margin-top:150px">Animate</button>

    <script src="my.js"></script>
</body>

</html>