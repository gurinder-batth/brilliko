<?php
namespace App;

class Assets {
 
    public function __construct()
    {
        wp_enqueue_media();
    }

    
    public function registerCss()
    {
         $this->cssFiles =  [
                                    //         [
                                    //    //     'name' => 'bulma.min.css'  ,
                                    //         'name' => 'mystyle.css'  ,
                                    //         ]
                                    ];
    }

    public function registerJs()
    {
        $this->jsFiles =  [  
                                            [
                                            'name' => 'myjs.js' 
                                            ]
                                     ];
    }

}
