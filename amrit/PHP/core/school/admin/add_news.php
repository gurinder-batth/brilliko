<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="style.css">
</head>
<body>
    
    <?php
     include_once 'header.php';
     include_once '../connect.php';
     ?>
     

     <h1>
         Add News Panel
          <div>
                  <form action="add_news_post.php" method="POST">
                              <div>
                                     <label for="">News Title</label>
                                     <input type="text" class="form-control" name="title">
                              </div>
                              <div>
                                     <label for="">News Description</label>
                                     <textarea class="form-control" name="description" placeholder="e.g. Hello world" col="30" rows="20"></textarea>
                              </div>
                              <div>
                                      <button>Submit</button>
                              </div>
                  </form>
          </div>
     </h1>


<?php
    //  Select All News And Display
    // IF We Run Select Query -> tHen What mysqli_query Return ?
    // Object mysqli_result
    // With this object we can fetch rows (mysqli_fetch_assoc())
    $query = "SELECT * FROM notifications ORDER BY id DESC ";
    $results  = mysqli_query($con,$query);

 for($i = 0 ; $i < $results->num_rows ;  $i++) {
     $row  = mysqli_fetch_assoc($results);
?>
 <div>
                   <h1> <?= $row['title'] ?> </h1>
                   <p> <?= $row['description'] ?></p>
                   <p> <i>Time:<?= $row['created_at'] ?></i> </p>
 </div>

<?php
 }
 ?>


</body>
</html>