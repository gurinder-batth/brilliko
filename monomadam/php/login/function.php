<?php
function isLogined(){
            // It's means only loginned user can access the profile
            if( ! isset($_SESSION['user']) ){
                header("Location:login.php");
                exit;
            }
}