<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
   public function home(Request $request)
   {
      $posts = Post::inRandomOrder()->paginate(20);
      return view("public.home")->withPosts($posts);
   }
}
