<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Foreach</title>
</head>
<body>
<?php

$students =  [

    'S1' ,
    'S2' ,
    'S3' ,
    'S4' 
];
?>



<ul>
    <?php foreach($students as $i => $student) { ?>
           <li> <?php echo $i . " --- " . $student; ?> </li>
    <?php } ?>
 </ul>


</body>
</html>