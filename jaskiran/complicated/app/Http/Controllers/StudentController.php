<?php

namespace App\Http\Controllers;

use App\ExamStudent;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function passStudent(Request $request)
    {
        $marks = ">=";
        if($request->fail){
              $marks = "<=";
        }
        $exams =  ExamStudent::whereExamId(1)
             ->join("exam_result","exam_students.id","=","exam_result.exam_student_id")
              ->where('exam_result.marks', $marks, 33)
                 ->get();

        return view("exams",['exams' => $exams]);
    }


    public function student(Request $request,ExamStudent $examstudent)
    {
        
        /** 
            *Here We Build Query On 
             @var $examstudent 
            *object of ExamStudent
         */
        $examstudent = $examstudent->whereExamId(1);

         /** Now We Check if user select all students then ignore the filter */
        if($request->res != "all"){
               
                 /** Here we change the scope of request var 
                  * Becuase in clourse method
                   @ $request normal var 
                  * Not reconized by php complier 
                  */
                 $this->request = $request;
                /**
                 * whereHas method takes the 
                   relationship 
                 *name as a first argument &
                  clourse method
                 *take as a second argument which get $query varible automatially
                 * by laravel
                 */
                $examstudent = $examstudent->whereHas("result",function($query){

                    /**  Here the 
                     $query var belongs to result table or model
                     *we need to just say fetch those who are marks > or < then 33
                     */
                    $query->where("marks","<",33);

                });

        }

        /**
         * Here in the end invoke get method on
          @ $examstudent var
         */
        $exams = $examstudent->get();

        
        return view("exams2",['exams' => $exams]);


ExamStudent::whereExamId(1)
    ->join("exam_result","exam_students.id","=","exam_result.exam_student_id")
        ->where('exam_result.marks', $marks, 33)
          ->get();



$examstudent->whereExamId(1)->whereHas("result",function($query){
                        $query->where("marks","<",33);                                         
                                        })->get();


    }


       
      
      
}




  