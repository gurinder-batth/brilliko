<div class="col-md-12">
     <table class="table table-bordered">
         <tr>
             <th>ID</th>
             <th>Name</th>
             <th>Number</th>
             <th>Schulde</th>
         </tr>
         @foreach ($flights as $flight)
             <tr>
                 <td>{{$flight->id}}</td>
                 <td>{{$flight->name}}</td>
                 <td>{{$flight->number}}</td>
                 <td> <a href="{{route('flight.schulde',['id' => $flight->id])}}">Schulde</a> </td>
             </tr>
         @endforeach
     </table>
</div>