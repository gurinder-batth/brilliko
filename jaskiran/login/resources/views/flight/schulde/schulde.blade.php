<div class="col-md-12">
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Departure</th>
            <th>Departure At</th>
            <th>Arrive</th>
            <th>Arrive At</th>
            <th>Delete</th>
            <th>Update</th>
        </tr>
        @foreach ($schuldes  as $schulde)
            <tr>
                <td>{{$schulde->id}}</td>
                <td>{{$schulde->departure}}</td>
                <td>{{date('H:i a D-M-y',strtotime($schulde->departure_at))}}</td>
                <td>{{$schulde->arrive}}</td>
                <td>{{$schulde->arrive_at}}</td>
                <td> <a href="{{route('flight.schulde.delete.confirmation',['id' => $schulde->id])}}" class="btn btn-danger btn-sm">Delete</a> </td>
                <td data-toggle="modal" data-target="#updateModel" onclick="updateSchuldeForm('{{$schulde}}')">Update</td>
            </tr>
        @endforeach
    </table>

    @include('flight.includes.update')

  

</div>