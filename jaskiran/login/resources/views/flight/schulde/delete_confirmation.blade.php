@extends('master')
@section('content')
<div class="col-md-12">
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Departure</th>
            <th>Departure At</th>
            <th>Arrive</th>
            <th>Arrive At</th>
        </tr>
            <tr>
                <td>{{$schulde->id}}</td>
                <td>{{$schulde->departure}}</td>
                <td>{{date('H:i a D-M-y',strtotime($schulde->departure_at))}}</td>
                <td>{{$schulde->arrive}}</td>
                <td>{{$schulde->arrive_at}}</td>
            </tr>
    </table>
</div>

      <div class="col-md-6">
        
        <form action="{{route('flight.schulde.delete')}}" method="post">
            {{ csrf_field() }}
            {{method_field('DELETE')}}
              <input type="hidden" name="id" value={{$schulde->id}}>
              <input type="hidden" name="previous_url" value={{URL::previous()}}>
                      <button class="btn btn-danger">Delete</button>  

                 &nbsp
                <a href="{{URL::previous()}}"><button class="btn btn-success" type="button">Discard</button></a>
    </form>
      </div>

@endsection