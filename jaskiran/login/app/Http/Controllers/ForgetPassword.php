<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\ForgetPasswordAlert;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ForgetPassword extends Controller
{
    // Return the Simple Email Enter Form
     public function form()
     {
         return view('admin.forget');
     }

    //   Forget Request
     public function forgetPasswordRequest(Request $request)
     {
            $admin = Admin::whereEmail($request->email)->first();

            if($admin == null){
                      session()->flash("unsuccess","Sorry we don't find account on our website with this email address");
                      return redirect()->back();
            }

              $admin->token = Str::random(60);
              $admin->save();
              $this->sendResetVerificationEmail($admin);
              session()->flash("success","Thank You! Reset Password link send on your registered email address");
              return redirect()->back();
     }

    // Send Email 
     public function sendResetVerificationEmail(Admin $admin)
     {
             Mail::to($admin)->send(new ForgetPasswordAlert($admin));
     }

    //  Return reset Password Form
    
    public function resetPasswordForm(Request $request)
    {
        
        if( $request->token == null)  {
            abort(404);
         }

         return view('admin.reset_password' )
                             ->withToken($request->token)->withId($request->id);
    }

    //  Request to Reset Password

    public function resetPassword(Request $request)
    {
          $admin = Admin::find($request->id);

          if($admin->token == null){
              abort(404);
          }

          $request->validate([
          
             'password' => 'required|min:3|max:10|confirmed' ,
             'token' => 'required' ,
             'id' => 'required'

          ]);

        //   IF TOKEN SUCCESSFULLY MATCHED 
          if($admin->token == $request->token){

            $admin->token = null;
            $admin->password = bcrypt($request->password);
            $admin->save();
            Auth::guard('admin')->loginUsingId($admin->id);
            return redirect()->route('admin.dashboard');
            
         }

        abort(404);
        
          

    }

}
