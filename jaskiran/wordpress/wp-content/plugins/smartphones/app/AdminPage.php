<?php
namespace App;

use Library\DefaultConfiguration;
use App\Controllers\Scrap;
use App\Controllers\Products;

class AdminPage extends DefaultConfiguration {

        public $page = [];

        //Here You Can Define The Controller Class Object Before Calling registerPage() Method
        public function __construct()
        {
           $this->scrap = new Scrap();
           $this->products = new Products();
            $this->registerPage();
        }

        // Register The Page Here
        // registerPage() Method use to store the page array in public property $page
        public function registerPage()
        {

            // Define The Main Page in $this->page
             $this->page = [
                   
                                //SCRAP PAGE ARRAY                
                                [
                                    'page_title'  =>  'Smartphones' , 
                                    'menu_title' => 'Smartphones' , 
                                    'capability' => 'manage_options'  ,
                                    'menu_slug' => $this->prefix_url . 'scrap' ,
                                    'function' =>   [$this->scrap,'index']  
                                ] ,
                               
               ];


            // Define The Sub Page in $this->sub_page
             $this->sub_page = [
                   
                                //SCRAP PAGE ARRAY                
                                [
                                    'parent_slug' => $this->prefix_url . 'scrap' ,
                                    'page_title'  =>  'Scarp All Products' , 
                                    'menu_title' => 'Scrap All Products' , 
                                    'capability' => 'manage_options'  ,
                                    'menu_slug' => $this->prefix_url . 'scrap' ,
                                    'function' =>   [$this->scrap,'index']  
                                ] ,
                               
                                //PRODUCTS PAGE ARRAY                
                                [
                                    'parent_slug' => $this->prefix_url . 'scrap' ,
                                    'page_title'  =>  'Scrap Product Landing' , 
                                    'menu_title' => 'Scrap Product Landing' , 
                                    'capability' => 'manage_options'  ,
                                    'menu_slug' => $this->prefix_url . 'products' ,
                                    'function' =>   [$this->products,'index']  
                                ] ,
                               
               ];





        }
    

}