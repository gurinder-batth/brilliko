@extends('master')

@section('title','Contact Page')


@section('content')
        {{-- @if($errors->any())
                  <span class="hsa-text-danger"> {{$errors->first()}}  </span>
           @endif --}}

           @if(session()->has('sucesss'))
              <div class="notification is-primary"> 
                <strong>   {{ session()->get('sucesss') }}       </strong>
               </div>
           @endif


             <form action="{{route('contact.post')}}" method="POST">


                {{ csrf_field() }}

                <div class="field">
                    <label class="label">Name</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Name" name="name">
                      @if($errors->has('name'))
                        <span class="hsa-text-danger"> {{$errors->first('name')}}  </span>
                      @endif
                    </div>
                  </div>

                  <div class="field">
                    <label class="label">Email</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Name" name="email">
                      @if($errors->has('email'))
                      <span class="hsa-text-danger"> {{$errors->first('email')}}  </span>
                    @endif
                    </div>
                  </div>

                  <div class="field">
                    <label class="label">Message</label>
                    <div class="control">
                     <textarea  id="" cols="30" rows="10" class="textarea" name="message"></textarea>
                     @if($errors->has('message'))
                     <span class="hsa-text-danger"> {{$errors->first('message')}}  </span>
                   @endif
                    </div>
                  </div>

                  <div class="field">
                    <div class="control">
                       <button class="button is-link">Submit</button>
                    </div>
                  </div>
                  

             </form>

@endsection