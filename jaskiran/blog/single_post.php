<?php include_once 'includes/header.php'; ?>

<?php include_once 'includes/categories.php'; ?>



<div class="section">
    <div class="conatiner">

        <div class="columns is-multiline">

                                        <div class="column is-12">
                                            <?php $post = get_post($_GET['post_id']); ?>
                                            <div class="quicksand">
                                                <h1 class="title" style="margin-left:25px">
                                                    <?= $post['title'] ?>
                                                </h1>
                                                <figure class="image">
                                                    <img src="<?= $post['img'] ?>" class="" alt="" style="padding:2rem">
                                                </figure>
                                                <div class="subtitle is-size-5" style="margin-left:25px">
                                                    <?= $post['description'] ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="column is-6">
                                        <article class="media" style="margin-left:25px">
                                                    <figure class="media-left">
                                                        <p class="image is-64x64">
                                                        <img src="https://bulma.io/images/placeholders/128x128.png">
                                                        </p>
                                                    </figure>
                                                    <div class="media-content">
                                                        <div class="content">
                                                        <p>
                                                            <strong>Gurinder</strong>
                                                            <br>
                                                          Nice.
                                                            <br>
                                                            <small>3 hrs</small>
                                                        </p>
                                                        </div>
                                                    </div>
                                                    </article>
                                        </div>

        </div>
    </div>
</div>


<?php require_once 'includes/footer.php'; ?>