<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FirstController extends Controller
{
       public function main()
       {
         $xyz = "Gurinder";
         return view("first")->withName($xyz);
       }
}
