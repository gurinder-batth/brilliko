<?php

namespace App\Http\Controllers;

use App\Payment;
use Instamojo\Instamojo;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{

    public function form()
    {
        return view("order");
    }


   public function order(Request $request)
   {

      $api = new  Instamojo("test_66ffa9d49bc4c2f0c89618e9fb0", "test_740244c755ecb2183ff10b479e8","https://test.instamojo.com/api/1.1/");


            try {
                $response = $api->paymentRequestCreate(array(
                    "purpose" => "Default",
                    "amount" =>  $request->amount ,
                    "send_email" => true,
                    "email" => $request->email ,
                    "redirect_url" => route('check.order')
                    ));

               Payment::insert([
                     "amount" => $response['amount'] ,
                     "email" => $response['email'] ,
                     "payment_id" => $response['id'] ,
               ]);

               return redirect()->to($response['longurl']);
                
            }
            catch (Exception $e) {
                print('Error: ' . $e->getMessage());
            }
   }


   public function checkOrder(Request $request)
   {

      $api = new  Instamojo("test_66ffa9d49bc4c2f0c89618e9fb0", "test_740244c755ecb2183ff10b479e8","https://test.instamojo.com/api/1.1/");
     
      try {
            $response = $api->paymentRequestPaymentStatus($request->payment_request_id,$request->payment_id);
            if($response['status'] == "Completed"){
                $payment = Payment::wherePaymentId($response['id'])->first();
                if($payment != null){
                    $payment->paid = 1;
                    $payment->save();

                    return "Thank You!";
                }
            }
        }
        catch (Exception $e) {
            print('Error: ' . $e->getMessage());
        }


   }
}
