<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Automation Bhutt Sarra :)
// Thank U :)
// Dash U :)
// Smile Please :)


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("sign-up","UserController@signup");
Route::post("login","UserController@login");

Route::middleware('auth:api')->group(function(){

    Route::get("dashboard","UserController@dashboard");
    
});