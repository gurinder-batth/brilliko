@extends('books.master')

@section("content")

<form action="{{route('books.insert')}}" method="post">

    {{ csrf_field() }}

    <div class="field">
        <label for="">Book</label>
        <input type="text" class="input" name='name' required> 
    </div>
    <div class="field">
        <label for="">Author</label>
        <input type="text" class="input" name='author'    required> 
    </div>
    <div class="field">
        <label for="">Price</label>
        <input type="number" class="input" min="1" name='price'   required> 
    </div>

    <div class="field">
        <button class="button is-link">Submit</button>
    </div>

</form>


@endsection