<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Object With Array</title>
</head>
<body>
    
    <script>
    
    // Object Name
               var country = {

                // Property:Property Value { Array }
                // Property:Property Value { String }
                // Property:Property Value { Number }
                    
                  states:[
                      'Punjab' ,
                      'Delhi' ,
                      'ETC'
                  ]         ,

                  cities:[
                      'Sangrur' ,
                      'Ludhiana',
                      'ETC'
                  ]
                        

               }

               for(var i = 0 ; i < country.states.length ; i++){

                         console.log(country.states[i])

               }

    </script>

</body>
</html>