var employes = [


    {
        name:"Sunil Nanda" ,
        pos:"Managing Director",
        img:"images/team1.jpg" 
    } ,
    {
        name:"William" ,
        pos:"Web & Graphic Designing Trainer",
        img:"images/team4.jpg" 
    } ,
    {
        name:"Harsimran Singh" ,
        pos:"Animation & VFX Trainer",
        img:"images/team3.jpg" 
    } ,
    {
        name:"Shweta Chhabra" ,
        pos:"Career Counselor",
        img:"images/team2.jpg" 
    } ,
    {
        name:"Gurinder Batth" ,
        pos:"Web Programming & Trainer",
        img:"images/gurinder.jpg" 
    } ,
    {
        name:"Anju Goyal" ,
        pos:"Business Developer",
        img:"images/team9.jpg" 
    } ,
    {
        name:"Sanjeev Mehra" ,
        pos:"Digital Marketing Trainer",
        img:"images/team6.jpg" 
    } ,
    {
        name:"Pankaj Gulati" ,
        pos:"Graphic Designer",
        img:"images/team7.jpg" 
    } ,


];

let slider = document.getElementById('js-slider');

var offset = 3;

let defaultMobileTab = 0;

let invoke = true;

let mobile = false;

function displaySlide(skip){
slider.innerHTML = "";
for(let i = skip ; i < skip + offset ; i ++) {

   let emp = employes[i];

   if(emp == null){
       return false;
   }

    let parentDiv = document.createElement("DIV");

    if(mobile){
            if(i  == 4){
                parentDiv.classList.add("gurinder-effect")
            }
            if(i  == 1){
                parentDiv.classList.add("gurinder-effect")
            }
    }

    let img = document.createElement("IMG");
    let captionDiv = document.createElement("DIV");
    let h4 = document.createElement("H4");
    let p = document.createElement("P");

   parentDiv.classList.add('parent-slide');

    img.src = emp.img;
    captionDiv.classList.add("caption");

    h4.innerHTML = emp.name;
    p.innerHTML = emp.pos;
    
    parentDiv.appendChild(img);

    captionDiv.appendChild(h4);
    captionDiv.appendChild(p);

    // Caption Div Attach with Parent Div
    parentDiv.appendChild(captionDiv);
    
    slider.appendChild(parentDiv);

}


}


function createTabs(){
       
        let parentDiv = document.createElement('DIV');

        parentDiv.classList.add("list");

        for (let i = 0; i < offset; i++) {
            let div = document.createElement('DIV');
            if(i == 0){
                div.classList.add('active-tab-color')
            }

            div.onclick = () => { 
                  for(let k = 0 ; k < 3 ; k ++){
                      console.log(k)
                        if(k == i){
                             parentDiv.childNodes[k].classList.add('active-tab-color');
                        } else {
                             parentDiv.childNodes[k].classList.remove('active-tab-color');
                        }
                  }

                  displaySlide( (i) * 3 ) 
            } 

            parentDiv.appendChild(div);
        }

        slider.parentNode.appendChild(parentDiv)

}


function createMobileTab(){
    
    let parentDiv = document.createElement('DIV');
    
    parentDiv.classList.add("list-tab-mobile");
     
    let next = document.createElement('BUTTON');
    let previous = document.createElement('BUTTON');

    next.innerHTML = "Next";
    previous.innerHTML = "Previous";

     previous.style.display = "none";
  

    next.onclick = () =>
     {
        
           defaultMobileTab++;

           if(defaultMobileTab > employes.length - 2){
                next.style.display = "none";
           } 

           if(defaultMobileTab > 0){
               previous.style.display = "block";
           }

           displaySlide(defaultMobileTab); 

     } 

     previous.onclick = () =>
     {
             defaultMobileTab--;

           if(defaultMobileTab <= 0){
                previous.style.display = "none";
           } 

           if(defaultMobileTab > 0){
                 next.style.display = "block";
           } 


            displaySlide(defaultMobileTab); 
     } 
    
    
    parentDiv.appendChild(next)
    parentDiv.appendChild(previous)

    slider.parentNode.appendChild(parentDiv)

}


function run(){
    if(window.innerWidth > 600){
        mobile = true;
        displaySlide(0);
         if(invoke){
              createTabs();
              invoke = false;
         }
    } else {
        offset = 1;
        displaySlide(defaultMobileTab);
        createMobileTab();
    }
}

window.addEventListener("resize", () => {
      run();
});

run();