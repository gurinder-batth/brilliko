function load_students(){

    document.getElementById('message').innerHTML = "Please Wait.......";

    // HTTP or HTTPS
        var request = new  XMLHttpRequest();
   
        // Init Values
        request.open("GET","students.php");

        // Method Overiding 
        request.onload = function(){
            document.getElementById('message').innerHTML = "Your data is loaded";
            document.getElementById('students').innerHTML = request.response;
        }

        request.send();

}