<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>News</title>
</head>
<body>



<?php  include_once 'header.php'; ?>
<?php  include_once 'connect.php'; ?>

<?php
    //  Select All News And Display
    // IF We Run Select Query -> tHen What mysqli_query Return ?
    // Object mysqli_result
    // With this object we can fetch rows (mysqli_fetch_assoc())
    $query = "SELECT * FROM notifications ORDER BY id DESC LIMIT 3";
    $results  = mysqli_query($con,$query);

 for($i = 0 ; $i < $results->num_rows ;  $i++) {
     $row  = mysqli_fetch_assoc($results);
?>
 <div>
                   <h1> <?= $row['title'] ?> </h1>
                   <p> <?= $row['description'] ?></p>
                   <p> <i>Time:<?= $row['created_at'] ?></i> </p>
 </div>

<?php
 }
 ?>

 <?php  include_once 'footer.php'; ?>

</body>
</html>