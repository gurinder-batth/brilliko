<?php

function load_my_scripts(){

    wp_enqueue_style("css1",get_template_directory_uri() . "/style.css");
    wp_enqueue_style("bootstrap","https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css");
    wp_enqueue_script("my",get_template_directory_uri() . "/my.js","1.0",true);
}


function mytheme_register()
{
    register_nav_menus( 
        
        [
            //  KeyName => Wordpress Label Name which show In Wp Menus    
            "header_menu" => "Header Menu" ,
            "foter_menu" => "Footer Menu" ,
        ]
     );
     
     add_theme_support( 'post-thumbnails' );
     add_theme_support( 'post-formats',['aside',"gallery","image","link"]);
}

function hello_world_rko_callback($args){
    return "<h1>Hello World!" . $args["name"] . "</h1>"; 
}


add_shortcode("hello_world_rko","hello_world_rko_callback");

// What is this
add_action("wp_enqueue_scripts","load_my_scripts");

// This hook fire on after setup theme
add_action( 'after_setup_theme', 'mytheme_register');


function my_widgets_here() {

    // Madam ji small chars of id :)
	register_sidebar( [
         'name' => "Side Categories" ,
         'id' => "sidecategoriesgurinder" ,
         'before_widget' => '<div>',
         'after_widget'  => '</div>',
         'before_title'  => '<h2 class="rounded">',
         'after_title'   => '</h2>',
    ] );


}

//  

function register_my_things()
{
    $post_type = "books";

    $label = [
        'name'               =>  'Books',
		'singular_name'      =>  'Book', 
		'menu_name'          =>  'Books', 
		'name_admin_bar'     =>  'Book', 
		'add_new'            =>  'Add New', 
		'add_new_item'       => 'Add New Book',
		'new_item'           => 'New Book', 
		'edit_item'          => 'Edit Book', 
		'view_item'          => 'View Book', 
		'all_items'          => 'All Books', 
		'search_items'       => 'Search Books', 
		'parent_item_colon'  => 'Parent Books:', 
		'not_found'          => 'No books found.', 
		'not_found_in_trash' => 'No books found in Trash.',
    ];


    $args = [

        'labels'   => $label,
        'description'        =>  "Add New Books",
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'book' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 2,
		'supports'           => [ 'title', 'editor', 'author', 'thumbnail', 'excerpt','custom-fields'], 

    ];

    register_post_type( $post_type, $args );
}



add_action( 'widgets_init', 'my_widgets_here' );

add_action( 'init', 'register_my_things' );


