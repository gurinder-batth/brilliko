@extends('master')


@section('title')
     Add Post
@endsection

@section('content')


<div class="jumbotron text-center">
  <h1>Post</h1>
</div>

@if($errors->any())

<div class="alert alert-danger">
    <strong>
        {{$errors->first()}}
    </strong>
</div>

@endif

@if(session()->has("success"))

 <div class="alert alert-success">
         <strong>  {{session()->get("success")}} </strong>
 </div>

@endif
  
<div class="container">

  <a href="{{route('post.recent')}}">Recent Posts</a> <br>

<form action="{{route('post.update')}}" method="post">

 {{ csrf_field() }}

<div class="form-group">
  <label for="">Title</label>
  <textarea name="title" class="form-control"  id="" cols="30" rows="2" >{{$post->title}}</textarea>
 </div>

 <div class="form-group">
  <label for="">Description</label>
  <textarea name="description" id="" class="form-control" cols="30" rows="4" >{{$post->description}}</textarea>
 </div>

 <input type="hidden" name="id" value="{{$post->id}}">

 <div class="form-group">
 <button class="btn btn-primary">Submit</button>
 </div>
 
</form>
 
</div>

@endsection