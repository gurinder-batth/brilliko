<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
     public function roles()
     {
         return  [

            "add_enquiry" => true ,
            "enquires_list" => true,
            "followups" => true,

         ];
     }
     public function rolesHR()
     {
         return  [

            "enquires_list" => true,
            "followups" => true,

         ];
     }
}
