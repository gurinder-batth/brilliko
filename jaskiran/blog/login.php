<?php include_once 'includes/header.php'; ?>
<?php include_once 'functions/functions.php'; ?>
<div class="section">
    <div class="conatiner">

      <div class="box">
      <form action="ajax.php" method="post">
              <div class="field">
                      <center>   <img src="https://cdn0.iconfinder.com/data/icons/flat-security-icons/512/lock-open-blue.png" alt="" width="100px"></center>
            </div>
          
            <?php if(session_has("login_error")) { ?>
                <div class="field">
                   <span class="subtitle has-text-danger"><?= session_once('login_error') ?></span>
               </div>
             <?php } ?>

            <div class="field">
                <p class="control has-icons-left has-icons-right">
                    <input class="input" type="email" name="email" placeholder="Email">
                    <span class="icon is-small is-left">
                        <i class="fas fa-envelope"></i>
                    </span>
                </p>
            </div>
            <div class="field">
                <p class="control has-icons-left">
                    <input class="input" type="password" placeholder="Password" name="password">
                    <span class="icon is-small is-left">
                        <i class="fas fa-lock"></i>
                    </span>
                </p>
            </div>
            <input type="hidden" name="method" value="login">
            <div class="field">
                <input type="submit" class="is-rounded button is-info" value="Login">
            </div>
        </form>

        <div class="field is-pulled-right">
                <a class="has-text-dark"><strike class=""><i class="fas fa-user-plus"></i><span class="quicksand "><span class="is-size-5">Line</span><span class="is-size-4 has-text-weight-bold has-text-info">Break</span></span>
      </strike></a>
            </div>
      </div>

    </div>
</div>

<?php require_once 'includes/footer.php'; ?>