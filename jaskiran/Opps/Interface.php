<?php

// Interface tells what to do rather than how to do it?

Interface PaymentGateway {

     function makePayment();

}

class Matsercard implements PaymentGateway{

    public function makePayment()
    {

    }

}

class Paytm implements PaymentGateway{
    
    public function makePayment()
    {
        # code...
    }

}

$paytm = new Paytm();
$paytm->makePayment();

$Matsercard = new Matsercard();
$Matsercard->makePayment();