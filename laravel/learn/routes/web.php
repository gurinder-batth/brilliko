<?php

// First Par Route URL
// Second Par View Name
// Route::view("/","home");

// Route::view("contact","contact");

Route::get("login","UserController@loginForm")->name("login");
Route::get("register","UserController@registerForm")->name("register");

Route::post("register","UserController@register")->name("register.request");

Route::post("login","UserController@login")->name("login.request");

Route::get("/","FirstController@main");



Route::middleware("auth")->group(function(){

    // Post ROutes STart Here ++++++++

Route::get("post","PostController@addForm")->name("post.add.form");

Route::post("post-add","PostController@add")->name("post.add");

Route::get("recent_post","PostController@get")->name("post.recent");


Route::get("edit/{id}","PostController@edit")->name("post.edit");
Route::post("update","PostController@update")->name("post.update");
Route::get("delete/{id}","PostController@delete")->name("post.delete");

// Post ROutes End Here ++++++++


Route::get("logout","UserController@logout")->name("logout");

});
