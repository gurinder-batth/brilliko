<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reset Account Password</title>
</head>
<body>
    
    <p>
        <h1>Hello! {{$admin->mobile }} </h1>
         <a href="{{ route('password.reset.form',['id' => $admin->id , 'token' => $admin->token]) }}">Please click to this click for reset  your account password.</a>
    </p>

</body>
</html>