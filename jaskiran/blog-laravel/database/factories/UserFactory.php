<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Post;
use App\User;
use App\Comment;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt("password")  , // password
        'remember_token' => Str::random(10),
    ];
});



$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->text(30) ,
        'description' => $faker->text(230) ,
        'user_id' =>  rand(1,100) ,
    ];
});



$factory->define(Comment::class, function (Faker $faker) {
    return [
        'reply' => $faker->text(40) ,
        'post_id' =>  rand(1,100) ,
    ];
});
