<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <div class="profile-sidebar">
        <div class="profile-userpic">
            <img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
        </div>
        <div class="profile-usertitle">
            <div class="profile-usertitle-name">Username</div>
            <div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="divider"></div>
    <form role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
    </form>
    <ul class="nav menu">
        <li><a href="index.html"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
        @can("add_enquiry")
        <li><a href="{{route('enquiry.add.form')}}"><em class="fa fa-pencil">&nbsp;</em>Add Enquiry</a></li>
        @endcan
        @can("enquires_list")
        <li><a href="{{route('enquiry.list')}}"><em class="fa fa-table">&nbsp;</em>Enquiries</a></li>
        @endcan
        <li><a href="{{route('enquiry.followups')}}"><em class="fa fa-table">&nbsp;</em>Follow Ups</a></li>
        <li><a href="login.html"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
    </ul>
</div><!--/.sidebar-->