<?php

get_header();


if(isset($_GET['s'])){
  echo "<h1> Search Result : {$_GET['s']} <h1>";
}

if(have_posts()){

    while(have_posts()){

          the_post();
          ?>
          <h1> <?php the_title(); ?> </h1>
          <p>  <?php 
              
                the_content(); 
                the_time('l, F jS, Y'); the_category(', '); 
                 the_author_posts_link();
                  ?> </p>
        

  <?php

    }


}

get_footer();