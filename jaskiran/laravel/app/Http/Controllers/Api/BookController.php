<?php

namespace App\Http\Controllers\Api;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookController extends Controller
{

    
    public function main(Request $request)
    {
                $books =  Book::get();
                return response()->json($books);
    }

    public function update(Request $request)
    { 
         $book = Book::find($request->id);
         $book->author =  $request->author;
         $book->name = $request->name;
         $book->price = $request->price;
         $book->save();

         return response()->json($book);
    }


}
