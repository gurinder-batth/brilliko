<?php

use Twilio\Rest\Client;

require_once 'vendor/autoload.php';


add_action("wpcf7_before_send_mail", "wpcf7_do_something_else");  
function wpcf7_do_something_else($cf7) {
    // get the contact form object
    $wpcf = WPCF7_ContactForm::get_current();

    // if you wanna check the ID of the Form $wpcf->id

	$message = '';

	$message .= "NAME ". $_POST['your-name'] . "\n";
	$message .= "EMAIL " . $_POST['your-email'] . "\n" ;
	$message .= "SUBJECT " . $_POST['your-subject'] . "\n";
	$message .=  "MESSAGE " . $_POST['your-message'];


		$sid    = "AC4a4e25810729bbb3e3755d575dcc138c";
		$token  = "4f141f952fdc951f1bf91ca0d43f39c4";
		$twilio = new Client($sid, $token);

		$message1 = $twilio->messages
					->create("whatsapp:+919646848434", // to
							array(
								"from" => "whatsapp:+14155238886",
								"body" => $message
							)
					);


    return $wpcf;
}

// Return if accessed this file directly.
if( ! defined( 'ABSPATH' ) ) {
	return;
}

// Load init.php file.
require_once get_template_directory() . '/inc/init.php';

/*
* Do not edit any code of theme, use child theme instead
*/
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields($fields)
{

   $carts = 	WC()->cart->get_cart();

    $status = false;
    foreach($carts as $cart){
            if($cart['product_id'] == 31){
				$status = true;
			}
	}
	
	if($status == true){
	
	$fields['billing']['billing_availability'] = [

		'type' => 'text' ,
		'label' => 'Availability' ,
		'placeholder' => 'Availability' ,
		// 'options' => [] ,
		'class' => ['form-row-wide'] , //css class to input
		'label_class' => [] , //css class for label
		'required' => true , 
		'clear' => true , 
		'enabled' => true , 
		'order' => 5 , 
		'validate' => [] , 
		'custom' => true , 
		'show_in_order' => true , 
		'priority' => 10 , 

	];

}

	return $fields;
}