<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Post</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.css">

</head>
<body>

    <div class="container">

        <div id="message" class="has-text-info"></div>

        <div class="section">
             <div class="field">
                   <label for="">Name</label>
                  <input type="text" class="input" id="user">
             </div>
             <div class="field">
                   <label for="">City</label>
                  <input type="text" class="input" id="city">
             </div>
             <div class="field">
                    <button class="button is-link" onclick="sendRequest()">Submit</button>
              </div>
        </div>
    </div>

    <script src="post.js"></script>
    
</body>
</html>