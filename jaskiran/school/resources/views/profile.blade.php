@extends('master')

@section('content')

<div class="container">
   <h1>  Hi! {{Auth::user()->name}} </h1>
   <br>

   <a href="{{route('user.logout')}}">Logout</a>
</div>

@endsection