@extends('master')

@section('content')


@if($errors->any())
       <div class="alert alert-danger">
             {{$errors->first()}}
       </div>
@endif


 <form action="{{route('admin.register')}}" method="post">
    {{ csrf_field() }}
 
    <div class="form-group">
            <label for="">Mobile</label>
            <input class="form-control" name="mobile">
    </div>

    <div class="form-group">
            <label for="">Email</label>
            <input class="form-control" name="email">
    </div>

    <div class="form-group">
            <label for="">Password</label>
            <input class="form-control" name="password">
    </div>

    <div class="form-group">
          <button class="btn btn-primary">Register</button>
    </div>
</form>


<h1><a href="{{route('admin.login')}}"> Login</a></h1>

@endsection