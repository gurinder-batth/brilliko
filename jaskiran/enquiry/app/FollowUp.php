<?php

namespace App;

use App\Enquiry;
use Illuminate\Database\Eloquent\Model;

class FollowUp extends Model
{
    protected $fillable = ['follow_at','description','enquiry_id'];

    public function enquiry()
    {
        return $this->hasOne(Enquiry::class,"id","enquiry_id");
    }
}
