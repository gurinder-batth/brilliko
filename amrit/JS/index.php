<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Validations with Javascript</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <div class="container">
        <div class="center">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Login User</h3>
                </div>
                <div class="panel-body">
                    <form action="login.php" name="login_form" method="post" onsubmit="loginUser(event)">

                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" class="form-control" name="username">
                            <span class="text-danger" id="username_error"></span>
                        </div>

                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="text" class="form-control" name="password">
                            <span class="text-danger" id="password_error"></span>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">Login</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

<script src="my.js"></script>
</body>


</html>