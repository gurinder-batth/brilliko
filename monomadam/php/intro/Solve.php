<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>
<body>
    
    <?php

       function my_sum($values){
                   $sum = 0;
                  foreach($values as $item){
                   $sum = $sum + $item;
                  }
                  return $sum;
       }
 
             $students  = [

                   [
                       'name' => 'Student 1' ,
                       'marks' => [
                           10 , 39 , 49
                       ]
                   ]  ,

                   [
                       'name' => 'Student 2' ,
                       'marks' => [
                           50 , 29 , 49
                       ]
                   ] ,

                   [
                       'name' => 'Student 3' ,
                       'marks' => [
                           13 , 49 , 19
                       ]
                   ] ,


             ]


?>

<table border="2">
 <tr>
    <th>Name</th>
    <th>Marks</th>
 </tr>

<?php

foreach($students as $student){ ?>

  <tr>
           
           <td> <?= $student['name'] ?> </td>
           <td> <?= my_sum($student['marks']) ?> </td>

  </tr>
<?php
}
?>

</table>

</body>
</html>