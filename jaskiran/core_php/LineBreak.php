<?php

interface Favourite{

}

class LineBreak implements Favourite {

    public function hi()
    {
        echo "Hi i am LineBrak";
    }

}


class Automation implements Favourite{


    public function hi()
    {
        echo "Hi i am Automation";
    }
    
}

class Test{


    public function hi()
    {
        echo "Hi i am Test";
    }
    
}


class Animals{

    public function index(Favourite $object)
    {
        $object->hi();
    }

}


$animals = new Animals();
$animals->index(new Automation());