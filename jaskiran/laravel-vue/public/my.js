
var app = new Vue({
    el:"#app" ,
    data:{
        post:{
            title:null,
            description:null
        } ,
        errors:{
            title:null ,
            description:null
        } ,
        message:null,
    } ,
    methods:{
        /*===============================================================================*/
        insert(){

            this.message = null
        
             axios.post(insert_url,{
                 title:this.post.title,
                 description:this.post.description,
             })

            //  200 succes

            // 401 Unauth

            //404 not found


             .then( (r) => {
                
                 if(r.status){
                     this.message = "Data Inserted Successfully"
                     this.post.title = null;
                     this.post.description  = null;
                     this.reset(null)
            
                 }

             })

             .catch( (e) => {
                const errors = e.response.data.errors;
                this.reset(errors)
             })

            /* 
            
                  errors:{
                     
                    title:[
                        'required' ,
                        'required' ,
                    ]

                  } ,
                  message:"Given data invaild" ,


            */

           } ,
/*===============================================================================*/
           reset(errors){

                    if(errors.title) {
                        this.errors.title = errors.title[0];
                    } else {
                        this.errors.title = null;
                    }

                    if(errors.description) {
                        this.errors.description = errors.description[0];
                    } else {
                        this.errors.description = null;
                    }

           }
 /*===============================================================================*/
    }
})