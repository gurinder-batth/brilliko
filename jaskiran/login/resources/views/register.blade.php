@extends('master')

@section('content')


@if($errors->any())
       <div class="alert alert-danger">
             {{$errors->first()}}
       </div>
@endif


 <form action="{{route('register.post')}}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
            <label for="">Name</label>
            <input class="form-control" name="name">
    </div>

    <div class="form-group">
            <label for="">Email</label>
            <input class="form-control" name="email">
    </div>

    <div class="form-group">
            <label for="">Password</label>
            <input class="form-control" name="password">
    </div>

    <div class="form-group">
          <button class="btn btn-primary">Register</button>
    </div>
</form>

@endsection