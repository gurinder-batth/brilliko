@extends('admin.master')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
            <li class="active">Widgets</li>
        </ol>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Widgets</h1>
        </div>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default articles">
                <div class="panel-heading">
                    Latest News
                    <ul class="pull-right panel-settings panel-button-tab-right">
                        <li class="dropdown"><a class="pull-right dropdown-toggle" data-toggle="dropdown" href="#">
                                <em class="fa fa-cogs"></em>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <ul class="dropdown-settings">
                                        <li><a href="#">
                                                <em class="fa fa-cog"></em> Settings 1
                                            </a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">
                                                <em class="fa fa-cog"></em> Settings 2
                                            </a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">
                                                <em class="fa fa-cog"></em> Settings 3
                                            </a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <span class="pull-right clickable panel-toggle panel-button-tab-left"><em
                            class="fa fa-toggle-up"></em></span></div>
                <div class="panel-body articles-container">
                    @include('inc.errors')
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{$enquiry->name}}</td>
                        </tr>
                        <tr>
                             <th>Mobile</th>
                             <td>{{$enquiry->mobile}}</td>
                        </tr>
                        <tr>
                             <th>Message</th>
                             <td>{{$enquiry->description}}</td>
                        </tr>
                    </table>
                    
                </div>
                <div class="panel-body articles-container">
                      <h3>Latest FollowUps</h3>
                      @foreach ($enquiry->followups as $followup)
                          <div class="panel-body">
                                <p>{{$followup->description}}</p>
                                 <time> {{$followup->follow_at}} </time>
                          </div>
                      @endforeach
                </div>
                <div class="panel-body articles-container">
                       
                    <form action="{{route('enquiry.add.followup')}}" method="post">
                        {{ csrf_field() }}
                       <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Follow At</label>
                                        <input type="date" class="form-control" name="follow_at">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Follow Note</label>
                                        <input type="text" class="form-control" name="follow_description">
                                    </div>
                                </div>
                                <input type="hidden" name="enquiry_id" value="{{$enquiry->id}}">
                                
                                <div class="col-md-6">
                                        <div class="form-group">
                                           <button class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                       </div>

                    </form>
                  </div>
            </div>
            <!--End .articles-->

        </div>
        <!--/.col-->

        <div class="col-sm-12">
            <p class="back-link">Lumino Theme by <a href="https://www.medialoot.com">Medialoot</a></p>
        </div>
    </div>
    <!--/.row-->
</div>
<!--/.main-->
@endsection
