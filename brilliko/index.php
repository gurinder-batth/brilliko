<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, intial-scale=1, max-scale=1">
    <meta name="theme-color" content="#2293f3">
    <title>3D ANIMATION | GRAPHICS | WEB DESIGNING | VFX | GAMING | FILM MAKING INSTITUTE | BRILLIKO INSTITUTE OF MULTIMEDIA</title>
    <meta name="description" content="Best Multimedia institute in Ludhiana to learn 3D, VFX Visual Effects, Animation, Game Design, Web Designing, Cinematography, film making etc.">
    <meta name="keywords" content="3d animation courses, vfx courses, multimedia course, 3d animation courses in ludhiana, animation courses in ludhiana, animation institute, graphic designing courses, graphic design courses in ludhiana, graphic designing, graphic design courses, learn graphic designing, web designing course, web design classes, web development courses, web designing course in ludhiana, institute of multimedia,  brilliko">
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="7 days">
    <meta name="abstract" content="Brilliko The learning Hub">
    <meta name="author" content="Sunil Nanda, Brilliko" />
    <meta name="copyright" content="Brilliko, India" />
    <meta name="distribution" content="global">
    <meta name="expires" content="never">
    <meta name="generator" content="dreamweaver">
    <meta name="googlebot" content="noodp">
    <meta name="language" content="english">
    <meta name="reply-to" content="info@brilliko.com">
    <meta name="web_author" content="Brilliko">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
 <h1>This is header</h1>
    </header>
    <div class="container-fluid">
        <div class="row bg-element brilliko_bg">
            <div class="col-12 text-center">
                <img src="images/logo.png" class="logo" alt="">
                <h4 class="call">Call: 0161-4655666, 076965-66000 </h4>
            </div>
            <!--col-12-->
            <div class="col-12 text-center hidden">
                <img src="images/autocad.png" class="icons" alt="">
                <img src="images/autodesk.png" class="icons" alt="">
                <img src="images/photoshop.png" class="icons" alt="">
            </div>
            <!--col-12-->
            <div class="col-12 text-center slide-content hidden">
                <h4 class="text-white">Welcome to Brilliko</h4>
                <h1 class="text-white">Institute of Multimedia</h1>
                <p class="text-white">The multimedia courses at BRILLIKO, train students to live up to the industry standards by providing them wide range of courses. </p>
            </div>
            <!--col-12-->
            <div class="col-12 slide-content">
                <form id="form-cta-subscribe-3" class="form-inline "  action="https://brilliko.com/admindata/contact" method="POST">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Your Name" name="name" required>
                    </div><!-- .form-group end -->
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Your Email" name="email" required>
                    </div><!-- .form-group end -->
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" required>
                        <input type="hidden" name="center" value="Ludhiana">
                        <input type="hidden" name="qualification" value="---------">
                        <input type="hidden" name="thank" value="thank">
                    </div><!-- .form-group end -->
                    <div class="form-group">
                        <select class="form-control" name="course" required>
                            <option disabled selected hidden>Your Course</option>
                            <option>Animation</option>
                            <option>Visual Effects (VFX)</option>
                            <option>Film Making</option>
                            <option>Game Design</option>
                            <option>Web Design</option>
                            <option>Graphic Design</option>
                            <option>Sketching</option>
                        </select>
                    </div><!-- .form-group end -->
                    <div class="form-group">
                        <input type="submit" class="form-control red-btn" value="Grab Your Course">
                    </div><!-- .form-group end -->
                </form><!-- #form-cta-subscribe-2 end -->
            </div>
            <!--col-12-->
        </div>
        <!--row-->
    </div>
    <!--container-fluid-->
    <div class="container" id="about">
        <div class="row">
            <div class="col-12 text-center">
                <div class="section-title text-center">
                    <span class="blue">Let’s Begin</span>
                    <h2 class="blue">Grow with Brilliko</h2>
                    <img src="images/section-title-separator.png" alt="">
                    <p class="about grey"> Brilliko Institute of Multimedia is Providing Animation, Web designing, VFX & Multimedia education, Started in 2011 by a team of management and technical evangelists with over ten years of experience in the Animation & Visual effects production & training domain, Brilliko offers a wide array of training programs aimed at producing high quality trained manpower to fuel lndia's Media & Entertainment industry...</p>
                </div>
            </div>

            <div class="hidden">
                <div class="row">
                    <div class="col-4 pos-rel">
                        <img src="images/arrow-right-top.png" class="pos-top" alt="">
                    </div>
                </div>
                <div class="col-4">
                    <div class="icon_about">
                        <img src="images/1.png" alt="">
                        <h4>Beginner</h4>
                    </div>
                </div>
                <div class="col-4">
                    <div class="icon_about">
                        <img src="images/2.png" alt="">
                        <h4>Intermediate</h4>
                    </div>
                </div>
                <div class="col-4">
                    <div class="icon_about">
                        <img src="images/3.png" alt="">
                        <h4>Expert</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 pos-rel"></div>
                    <div class="col-4 pos-rel">
                        <img src="images/arrow-right-bottom.png" class="pos-btm" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--#about-->
    <!--services-->
    <div id="choosing-courses">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center nob">
                        <span class="text-white">Best Courses</span>
                        <h2 class="text-white">Choose Your Course</h2>
                        <img src="images/section-title-separator.png" alt="">
                    </div>
                </div>
                <div class="col-4">
                    <img src="images/works01.jpg" class="img-res zoom" alt="">
                    <div class="description">
                        <h4>Animation</h4>
                        <p> Animation is every where. Students loves to create their own character. </p>
                    </div>
                </div>
                <div class="col-4">
                    <img src="images/works02.jpg" class="img-res zoom" alt="">
                    <div class="description">
                        <h4>VFX (Visual Effects)</h4>
                        <p>Students learn to create sophisticated visual effects.. </p>
                    </div>
                </div>
                <div class="col-4">
                    <img src="images/works03.jpg" class="img-res zoom" alt="">
                    <div class="description">
                        <h4>Graphic Designing</h4>
                        <p>Graphic design, also known as communication design..</p>
                    </div>
                </div>
                <div class="col-4">
                    <img src="images/works04.jpg" class="img-res" alt="">
                    <div class="description">
                        <h4>Website Designing</h4>
                        <p>Game designing is very popular nowdays. Most of the illustrators did gaming and make their own game.</p>
                    </div>
                </div>
                <div class="col-4">
                    <img src="images/works05.jpg" class="img-res zoom" alt="">
                    <div class="description">
                        <h4>Game Designing</h4>
                        <p>This should be used to tell a story and let your users know about your product or service.</p>
                    </div>
                </div>
                <div class="col-4">
                    <img src="images/works06.jpg" class="img-res zoom" alt="">
                    <div class="description">
                        <h4>Film Making</h4>
                        <p>The film industry is bringing out fantastic films every week. The film industry is growing..</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end services-->

    <div class="container" id="team">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center nob">
                    <span class="text-red">Meet</span>
                    <h2 class="text-red">Our Team</h2>
                    <img src="images/section-title-separator.png" alt="">
                </div>
            </div>

            <!-- Slider Here -->
            <div class="col-12">
                     <div class="slides" id="js-slider">
                    </div>
            </div>

        </div>
    </div>

    <div id="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center nob">
                        <span class="text-white"> Best Students </span>
                        <h2 class="text-white">Great Testimonials</h2>
                        <img src="images/section-title-separator.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="slider">
                        <input type="radio" name="slider" title="slide1" checked="checked" class="slider__nav" />
                        <input type="radio" name="slider" title="slide2" class="slider__nav" />
                        <input type="radio" name="slider" title="slide3" class="slider__nav" />
                        <input type="radio" name="slider" title="slide4" class="slider__nav" />

                        <div class="slider__inner">
                            <div class="slider__contents">
                                <p class="slider__txt">Brilliko provides an excellent environment for creative thinking. I enjoy coming to the class everyday and i have a really good learning experience. Brilliko is the best in all terms.</p>

                                <img src="images/std1.jpg" class="img-test" alt="">
                                <p class="name">Harsh Gandhi</p>

                            </div>
                            <div class="slider__contents">
                                <p class="slider__txt">Innovation and creativity blended under one roof at Brilliko. I enjoy learning new things everyday.This is the best institute of multimedia in punjab. I am happy to be a part of Brilliko.</p>
                                <img src="images/std2.jpg" class="img-test" alt="">
                                <p class="name">Manisha Kapoor</p>
                            </div>
                            <div class="slider__contents">
                                <p class="slider__txt">I am proud to a part of brilliko institute. This is a best place for the freshers who want to learn multimedia courses. They provides a wonderfull enviornment to there students.</p>
                                <img src="images/std3.jpg" class="img-test" alt="">
                                <p class="name">Armann Grewal</p>
                            </div>
                            <div class="slider__contents">
                                <p class="slider__txt">Brilliko (Institute Of Multimedia) is the best institute in the Punjab. I recommended this institute who are searching for best placement in Punjab. I proud to be Brillikan.</p>
                                <img src="images/std4.jpg" class="img-test" alt="">
                                <p class="name">Farheen Saifi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--testimonials end-->
    <div class="section-content">
        <div class="container">
            <div class="row">
                <div class="col-2">
                    <img src="images/placement.png" class="img-res" alt="">
                </div>
                <div class="col-10">
                    <h2 class="orange">Getting Your Deam Job</h2>
                    <p class="text-ce">“Students of Brilliko are big part of India’s leading Designing Companies, Animation Industry and Film Studios as well as global media an entertainment firms. So what are you waiting for? take a great step, Join Brilliko!”</p>
                </div><!-- .col-md- end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>
    <!--contact-->
    <div class="section-content" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="col-12">
                        <div class="section-title text-center nob">
                            <span class="text-white"> Learn Great Skills Now </span>
                            <h2 class="text-white">Contact Us</h2>
                            <img src="images/section-title-separator.png" alt="">
                        </div>
                    </div>

                    <p class="text-center">Brilliko Institiute of Multimedia <br>
                        2nd Floor, Above Hemkunt Furniture, Near Bus Stand, Ludhiana
                    </p>
                    <p class="btn scroll-to x-large gradient mt-50 ">
                        Phone No: 0161-4655666, <br>
                        Mobile No: 076965-66000, 089686-52383
                        info@brilliko.com
                    </p>
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

    <footer id="footer">
        <div id="footer-bar-1" class="footer-bar">
            <div class="footer-bar-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-4">
                            <div class="fb-row">
                                <div class="copyrights-message">2018 - 2019 © <a href="#;"><span class="colored">brilliko.com</span></a>. All Rights Reserved.</div>
                            </div><!-- .fb-row end -->
                        </div><!-- .col-md-12 end -->
                        <div class="col-8">
                            <ul class="social-icons animated x4 grey hover-colorful icon-only">
                                <li><a class="si-facebook" href="https://www.facebook.com/brillikoinstitutes" target="_blank"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a></li>
                                <li><a class="si-twitter" href="https://twitter.com/brilliko" target="_blank"><i class="fa fa-twitter"></i><i class="fa fa-twitter"></i></a></li>
                                <li><a class="si-instagramorange" href="https://www.instagram.com/brillikomultimedia/" target="_blank"><i class="fa fa-instagram"></i><i class="fa fa-instagram"></i></a></li>
                                <li><a class="si-youtubeorange" href="https://www.youtube.com/channel/UC2asQ1FT45mSuCtS-2P_Ptw" target="_blank"><i class="fa fa-youtube"></i><i class="fa fa-youtube"></i></a></li>
                                <li><a class="si-googleplusorange" href="https://plus.google.com/u/0/114839433964864413122" target="_blank"><i class="fa fa-google-plus"></i><i class="fa fa-google-plus"></i></a></li>
                            </ul><!-- .social-icons end -->
                        </div>
                    </div><!-- .row end -->
                </div><!-- .container end -->
            </div><!-- .footer-bar-wrap -->
        </div><!-- #footer-bar-1 end -->
    </footer>
<script src="js/my.js"></script>
</body>

</html>