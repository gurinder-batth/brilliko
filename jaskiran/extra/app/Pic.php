<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pic extends Model
{

    // Mutator
    public function getPicsAttribute($value)
    {
       return json_decode($value);
    }
}
