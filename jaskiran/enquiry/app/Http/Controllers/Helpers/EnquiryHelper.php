<?php

namespace App\Http\Controllers\Helpers;

use App\Enquiry;
use App\FollowUp;
use App\LastFollowUp;

trait EnquiryHelper{

    public function addNewEnquiry() : Enquiry
    {
        return Enquiry::create([

            "mobile" => $this->req->mobile ,
            "name" => $this->req->name ,
            "description" => $this->req->description ,
            "status" => 1 , //New 

         ]);
    }

    public function createNewFollowUp(Enquiry $enquiry) : FollowUp
    {
       return FollowUp::create([
            "follow_at" => $this->req->follow_at ,
            "description" => $this->req->follow_description ,
            "enquiry_id" => $enquiry->id ,
         ]);
    }


    public function createLastFollowUp(FollowUp $followup)
    {
         LastFollowUp::updateOrCreate(
              [ "enquiry_id" => $followup->enquiry_id ] , //condtion
              [
                   "follow_up_id" => $followup->id ,//values
                   "follow_at" => $followup->follow_at ,
                   "enquiry_status" => $followup->enquiry->status ,
              ]
          );
    }

}