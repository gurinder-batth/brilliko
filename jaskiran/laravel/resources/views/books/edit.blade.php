@extends('books.master')

@section("content")

<form action="{{route('books.update')}}" method="post">

    {{ csrf_field() }}

    <div class="field">
        <label for="">Book</label>
        <input type="text" class="input" name='name' value="{{$book->name}}" required> 
    </div>
    <div class="field">
        <label for="">Author</label>
        <input type="text" class="input" name='author'   value="{{$book->author}}" required> 
    </div>
    <div class="field">
        <label for="">Price</label>
        <input type="number" class="input" min="1" name='price'  value="{{$book->price}}" required> 
    </div>

    <input type="hidden" name="id" value="{{$book->id}}">
    
    <div class="field">
        <button class="button is-link">Submit</button>
    </div>

</form>


@endsection