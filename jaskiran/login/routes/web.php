<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Flight Management

Route::namespace('FlightSystem')->group(function() {


    Route::prefix('flight')->group(function(){

         Route::get('/','FlightController@getFlights')->name('flight.get');
         Route::post('/','FlightController@flight')->name('flight.create');

         Route::prefix('schulde')->group(function(){

             Route::get('flight/{id}','FlightController@getFlightSchulde')->name('flight.schulde');
             Route::post('flight','FlightController@createSchulde')->name('flight.schulde.create');
             Route::get('delete_confirmation/{id}','FlightController@deleteConfirmation')->name('flight.schulde.delete.confirmation');
             Route::delete('delete','FlightController@deleteSchulde')->name('flight.schulde.delete');

             Route::patch('update','FlightController@updateSchulde')->name('flight.schulde.update');

         });

    });


});


















Route::view('/','welcome')->name('welcome');


Route::view("register",'register')->name('register');
Route::view("login",'login')->name('login');

Route::post('register','UserController@register')->name('register.post');
Route::post('login','UserController@login')->name('login.post');
Route::get('logout','UserController@logout')->name('logout');


Route::prefix('admin')->group(function() {

    Route::middleware('admin')->group(function(){

        Route::get('/','AdminController@dashboard')->name('admin.dashboard');
        Route::view('delete_user','admin.delete_user')->name('admin.delete_user');
        Route::view('profile','admin.profile')->name('admin.profile');

    });



    Route::prefix('password')->group(function(){


         Route::get('forget','ForgetPassword@form')->name('password.forget.form');

         Route::post('forget','ForgetPassword@forgetPasswordRequest')->name('password.forget');

         Route::get('reset/{token}/{id}','ForgetPassword@resetPasswordForm')->name('password.reset.form');

         Route::post('reset','ForgetPassword@resetPassword')->name('password.reset');


    });




    Route::prefix('otp')->group(function(){

       Route::get("login","OtpLogin@loginForm")->name("otp.login");
       Route::post("login","OtpLogin@otpSend")->name("otp.send");


       Route::get("verification","OtpLogin@otpForm")->name("otp.verification");

       Route::post("verification","OtpLogin@otpVerfication")->name("otp.verification.request");



   });

    // Login Form Get Method
    Route::get('login','AdminController@loginForm')->name('admin.login.form');
    // Register Form Get Method
    Route::get('register','AdminController@registerForm')->name('admin.register.form');


    Route::get('user_verfication/{id}/{token}','AdminController@verification')->name('admin.user_verification');



    // Login Post Request
    Route::post('login','AdminController@login')->name('admin.login');
    Route::post('register','AdminController@register')->name('admin.register');

    Route::get('logout','AdminController@logout')->name('admin.logout');

    Route::get('web_users','AdminController@websiteUsers')->name('admin.web_users');


});