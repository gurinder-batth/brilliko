<?php
namespace Library ;



/* 
 Request Class is a very important class for handle the Post Request . 
           HOW IT WORKS?
           1) Developer Define the three hidden Fields in Form
                a) method name
                b) class_name
                c) class_method

          2) Request Class create the property of Every $_Post key and Invoke the Controller 
            Method and Pass the Request Class Object into Method.

        3) Which is very Helpful for method Dependency Injection.
*/

class Request extends DefaultConfiguration {
    //   Default value of request property of Request Class is false
      private $request = false;
      
       public function __construct()
       {
           // Check The method key exit in $_POST Global Array Variable
            if(isset($_POST['method'])){
                // Check The Method Key and Developer Define Key Both Are Same
                // IF The Condition is True The Convert the Each key of $_POST  Array into
                // Request class property
                if($this->method == $_POST['method']){
                    // And Set The request Property True Because  with help of This we inject
                    //  The Famous Pattern of Oop which is called dependency injection
                       $this->request = true;
                        foreach($_POST as $key => $value){
                            $this->{$key}  = $value;
                        }
                }
            }

            // Here We Check if rqeuest property is true then Implement The Method dependency Injection
            //  Wow That's Great.......................... :)
            if($this->request){
                $classname  =  "\\App\\Controllers\\". $this->class_name;
                $classObject = new   $classname( );
                return $classObject->{$this->class_method}($this);
            }
       }

}

