<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LastFollowUp extends Model
{

   protected $dates = ['follow_at'];

   protected $fillable = ['enquiry_id','follow_up_id','follow_at','enquiry_status'];

   public function followup()
   {
       return $this->hasOne(FollowUp::class,"id","follow_up_id");
   }
}
