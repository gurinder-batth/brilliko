<?php
include_once 'header.php';
include_once 'connect.php';

$query  = "SELECT * FROM contact";
$results = mysqli_query($con,$query);


if($results->num_rows > 0){
?>
<table border="2" style="width:100%" cellpadding="10">
 <tr>
            <th>ID</th>
            <th>Name</th>
            <th>City</th>
            <th>Mobile</th>
  </tr>
<?php
    for($i = 0 ; $i < $results->num_rows ; $i++){

            $row = mysqli_fetch_assoc($results); //Return One Row At a Time
             
            ?>
                
                 <tr>
                    <td> <?= $row['id'] ?></td>
                    <td> <?= $row['name'] ?></td>
                    <td> <?= $row['city'] ?></td>
                    <td> <?= $row['mobile'] ?></td>
                    <td> <a href="update.php?id=<?= $row['id'] ?>">Update</a> </td>
                 </tr>


            <?php
    }

} else {
    echo "<h1>No Result Found</h1>";
}

?>
</table>

<?php
include_once 'footer.php';
?>