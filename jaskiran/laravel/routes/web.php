<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//   Route::get('/', function () {
     
//     return view('welcome');
    
//     });

// Route::get('contact','ContactController@index')->name('contact.form');

// Route::post('contact-request','ContactController@save')->name('contact.post');

// Route::get('contact_list','ContactController@list')->name('contact.list');


// // Get /Post /Delete /Patch /Put

// // Route::get('test',function(){

// //   echo "test";

// // });

// // Route::get('test','TestController@main');

// // Route::get('test',function(){
// //   return view('test');
// // });

// // Route::view('test','test');

// Route::prefix('admin')->group(function(){


// Route::view('test','test');
// Route::view('/','test');



// });


// Route::get('admin/add_user','Controller@method');
// Route::get('admin/delete_user','Controller@method');

// Route::get('report/today','Controller@method');
// Route::get('report/yesterday','Controller@method');


Route::get('books','BookController@main')->name('books.list');

Route::get('edit/{id}','BookController@edit')->name('books.edit');

Route::post('update_book','BookController@update')->name('books.update');


Route::get('delete/{id}','BookController@delete')->name('books.delete');

Route::post('insert','BookController@insert')->name('books.insert');

Route::view('insert','books.insert')->name('books.insert_form');





Route::prefix('json')->group(function(){

      Route::view('/','api.books');
      Route::view('books2','api.books2');
      Route::get('books','Api\BookController@main')->name('json.books.list');
      Route::post('update_book','Api\BookController@update')->name('json.book.update');

});


// Route::get('insert_form','BookController@insertForm')->name('books.insert_form');