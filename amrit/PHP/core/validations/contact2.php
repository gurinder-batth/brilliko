<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Contact</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>


    <div class="container">

      <?php if(isset($_SESSION['error'])) { ?>

        <div class="alert alert-danger">
             <strong> <?= $_SESSION['error']  ?> </strong>
        </div>

      <?php unset($_SESSION['error']); } ?>

        <form action="contact-handle.php" method="post">
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" class="form-control"   name="name" required>
            </div>
            <div class="form-group">
                <label for="">City</label>
                <input type="text" class="form-control"   name="city">
            </div>
            <div class="form-group">
                <label for="">Mobile</label>
                <input type="text" class="form-control"  name="mobile">
            </div>
            <div class="form-group">
                <label for="">Address</label>
                <input type="text" class="form-control"  name="address">
            </div>
            <div class="form-group">
                <button class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>

</body>

</html>