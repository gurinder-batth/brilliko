@extends('master')
@section('content')
        <div class="col-md-12">
                <form action="{{route('flight.create')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                                <label for="">Flight Name</label>
                                <input class="form-control" name="name">
                        </div>
            
                            <div class="form-group">
                                    <label for="">Flight Number</label>
                                    <input class="form-control" name="number">
                          </div>
            
                            <div class="form-group">
                                    <button class="btn btn-primary">Create</button>
                          </div>
                  </form>
        </div>
        @include('flight.flight.flights')
@endsection