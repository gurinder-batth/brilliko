@extends('master')

@section('title','Contact List')


@section('content')

<table class="table table-bordered">
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Email</th>
         <th>Message</th>
    </tr>
    
    @foreach ($contacts as $contact)
    
        <tr>
            <td>{{$contact->id}}</td>
            <td>{{$contact->name}}</td>
            <td>{{$contact->email}}</td>
            <td>{{$contact->message}}</td>
        </tr>

    @endforeach

</table>

{{$contacts->links( )}}
@endsection