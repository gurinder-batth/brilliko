<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['name','course_id','batch_id','phone','admission','marks'];
    public function course()
    {
        return $this->hasOne(Course::class,"id","course_id");
    }
    public function batch()
    {
        return $this->hasOne(Batch::class,"id","batch_id");
    }
}
