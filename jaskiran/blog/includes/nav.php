<nav class="navbar " role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="index.php">
      <strike class=""> <span class="quicksand "><span class="is-size-5">Line</span><span class="is-size-4 has-text-weight-bold has-text-info">Break</span></span>
      </strike>
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">

    <div class="navbar-end">

      <?php if(session_has('user')) { ?>
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          <?= session_get('user')['name'] ?>
        </a>

        <div class="navbar-dropdown">
        <a class="navbar-item" href="create_post.php">
           Create Post
          </a>
          <a class="navbar-item" href="ajax.php?method=logout">
                Logout
          </a>
        </div>
        <?php } else { ?>
        <a class="button is-light" href="login.php">
          <i class="fas fa-sign-in-alt"></i>Log in
        </a>
        <?php } ?>


      </div>


    </div>
</nav>