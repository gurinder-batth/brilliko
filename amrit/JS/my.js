
function v_required(val,name,obj){

    if( val.value.length == 0){
          obj.innerHTML = ` ${name} should be required`;
          return false;
   } 

   return true;
   
}
function v_min(val,name,obj){

    if( val.value.length  < 3){
          obj.innerHTML = ` ${name} should be more than 3 chars`;
        return false;
   }

   return true;
   
}


function loginUser(e){

e.preventDefault();

var username  =  document.login_form.username;
var username_error  =  document.getElementById('username_error');
var password  =  document.login_form.password;
var password_error  =  document.getElementById('password_error');

var v1 = v_required(username,"Username",username_error);
var v2 = v_required(password,"Password",password_error);

var v3 = v_min(username,"Username",username_error);
var v4 = v_min(password,"Password",password_error);


if(v1 ==   true && v2 == true && v3 == true && v4 == true){
      document.login_form.submit();
} 


}