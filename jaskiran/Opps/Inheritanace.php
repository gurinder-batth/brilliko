<?php

class Database {

    public $conn = 'connected';
     public function connect()
     {
         return $this->conn;
     }

}


class FetchStudents {



    public function all()
    {
        // Dependancy Injection
       $database = new Database();
        $conn = $database->connect();
        return  $conn;
    }


}



$students = new FetchStudents();
print_r($students->all());
