<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::middleware(['auth'])->group(function(){

Route::prefix("enquiry")->group(function(){

    Route::get("/","EnquiryController@addForm")->name("enquiry.add.form");
    Route::post("add","EnquiryController@addEnquriy")->name("enquiry.add");

    Rute::get("list","EnquiryController@list")->name("enquiry.list")
                                                ->middleware("can:enquires_list");

    Route::get("followups","EnquiryController@followups")->name("enquiry.followups");

    Route::get("enquiry/{id}","EnquiryController@enquiry")->name("enquiry.enquiry");
    
    Route::post("follow-add","EnquiryController@addFollowUp")->name("enquiry.add.followup");

});

Route::get('/home', 'HomeController@index')->name('home');

});