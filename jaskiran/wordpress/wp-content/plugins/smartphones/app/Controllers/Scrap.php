<?php
namespace App\Controllers;

use GuzzleHttp\Client;
use Library\DefaultConfiguration;
use Library\Session;
use Library\Request;
use DOMDocument;
use App\Helpers\ProductLandingPage;

class Scrap extends DefaultConfiguration{

     private $url = 'https://www.gsmarena.com/';
     public $products = [];

     public function __construct()
     {
        $this->categories();
     }

     public function index()
     {
        wp_enqueue_media();
         return view('home',[
            'categories' => $this->categories ,
            'method' => $this->method ,
            'session' => new Session()
         ]);
     }

     public function categories()
     {
            $this->categories = get_categories([
               'hide_empty' => FALSE
            ]);
     }

     public function fetchData(Request $request)
     {

      $session = new Session();
      $client = new Client();
      
   

      $res = $client->request('GET',$request->url);
      // $res  varibable contains the front page HTML
      $res = $res->getBody()->getContents();
      
      
      // 6-0-p5.php

      // Here i write the Regex of get the nav-pages
      $lastPageRegex = '|<div class="nav-pages">[\s\S]*<a href=".*">(\d)<\/a>|';
      preg_match_all($lastPageRegex,$res,$lastPage);

    


      $links = [];
      if(isset($lastPage[1][0])){
           $modify_url = substr(str_replace('.php','',$request->url),0,-2) . "f-";
           $digits = substr(str_replace('.php','',$request->url),-2);
           for($i = 0; $i < $lastPage[1][0] ; $i++){
              array_push($links,$modify_url . $digits . '-0-p' . ($i+1) . '.php');
           }
      } else{
         array_push($links,$request->url);
      }

     
      // foreach($links as $key => $link){
     
      // if($key > 0 ){
      //    $res = $client->request('GET',$link);
      //    // $res  varibable contains the front page HTML
      //    $res = $res->getBody()->getContents();
      // }

 
     // Inside Nav Pages there is a makers class
      $regex = '|<div class="makers">(\s\S*)*<br class="clearfix">|iU';
      preg_match_all($regex,$res,$data);
      $frontPage = str_replace('<br class="clearfix">',' ',$data[0][0]);
      

    
      //Now Finally Get All Products href
      $productsRegex = '|<li>(.*?)<\/li>|';

      preg_match_all($productsRegex,$frontPage,$products);

    

       $PLP = new ProductLandingPage();
       $PLP->request = $request;
       $PLP->client = $client;
       $PLP->url = $this->url;

        foreach($products[1] as $key => $product){
            $PLP->products = $products;
            $PLP->product = $product;
            $PLP->title($key);
            $PLP->getData();
            $PLP->insertPost();
        }

  //    }

       $session->create('success','Thank you!');
     }

}