<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.css">
    <style>

    </style>
</head>
<body>

    
    <div class="section">
        <div class="container">
             <div id="result">
             </div>
        </div>
    </div>


    <div class="modal" id="modal">
            <div class="modal-background"></div>
            <div class="modal-content">
              
                <div class="notification">
      
                <div id="success_messgae"></div>
                       
                <div class="field">
                        <label for="">Book</label>
                         <input type="text" class="input" id="modal_book">
                  </div>
               
                  <div class="field">
                        <label for="">Author</label>
                         <input type="text" class="input"  id="modal_author">
                  </div>
                  <div class="field">
                        <label for="">Price</label>
                         <input type="text" class="input"  id="modal_price">
                         <input type="hidden" class="input"  id="modal_id">
                  </div>
                  <div class="field">
                        <button class="button is-link" onclick="updateBook()">Update</button>
                  </div>

                </div>

        </div>
    <button class="modal-close is-large" aria-label="close" onclick="$('#modal').removeClass('is-active')"></button>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    
    // Steps
    /*

    1) Load Data 

      let res = [

                {
                           id:6,
                           book:"Book" ,
                           author:"judd",
                           price:23
                } ,
           
                {
                           id:6,
                           book:"Book" ,
                           author:"judd",
                           price:23
                },
           
                {
                           id:6,
                           book:"Book" ,
                           author:"judd",
                           price:23
                }
           

      ]

      res[2].book

      Then Create Table


      2) Open Model on ID <td> Element 

      3) And Invoke Update(book object , tr) method 

    */

      var tr = null;

       $(document).ready(invoke);

        // 1) Data Load
       var books_url = '{{route('json.books.list')}}'

       function invoke(){
          
            $.get(books_url, function(res,staus){
                  console.log(res)
                   var table = document.createElement("TABLE");
                    table.classList = "table is-bordered is-fullwidth"

                    for(let i in res){

                      let tr = document.createElement("TR");

                       let td1 = document.createElement("TD");
                       let td2 = document.createElement("TD");
                       let td3 = document.createElement("TD");
                       let td4 = document.createElement("TD");

                       td1.innerHTML =  res[i].id;

                        td1.onclick = function(){
                               update(res[i],tr);
                        };

                       td2.innerHTML =  res[i].name;
                       td3.innerHTML =  res[i].author;
                       td4.innerHTML =  res[i].price;

                       tr.appendChild(td1)
                       tr.appendChild(td2)
                       tr.appendChild(td3)
                       tr.appendChild(td4)

                       table.appendChild(tr)

                     
                    }

                  document.getElementById('result').appendChild(table)
                  
            })
            

            
        // var tr = document.createElement("TR");

        // var td1 = document.createElement("TD");

        // td1.innerHTML = "1";

        // var td2 = document.createElement("TD");

        // td3.innerHTML = "5667";

        // tr.appendChild(td1)
        // tr.appendChild(td2)
        


        


       }

       function  update(book,row){
        // console.log(book)
           tr = row;
           console.log(tr)
           $("#modal").addClass("is-active")
           $("#modal_book").val(book.name)
           $("#modal_author").val(book.author)
           $("#modal_price").val(book.price)
           $("#modal_id").val(book.id)
       }
    

      function updateBook(){
           let name = $("#modal_book")
           let author = $("#modal_author")
           let price = $("#modal_price")
           let id = $("#modal_id");

          var url = "{{route('json.book.update')}}"
           var data = {
                         id:id.val(),
                        name:name.val() ,
                        author:author.val() ,
                        price:price.val() ,
                        _token:'{{csrf_token()}}' ,
          };

              $.post(url,data,function(res,status,e){
             
                    if(e.status == 200){
                         $("#success_messgae").html("Book Successfully updated!");
                         
                           tr.children[1].innerHTML = name.val()
                           tr.children[2].innerHTML = author.val()
                           tr.children[3].innerHTML = price.val()
                         $("#modal").removeClass("is-active")
                    }

              })

            //     $.ajax({
            //         url:url,
            //         type:"POST",
            //         data:data,
            //         //contentType:"application/json",
            //         headers:{"X-CSRF-TOKEN":"{{csrf_token()}}"} ,
            //         success: function(r){
            //             console.log(r)
            //         } ,
            //         error:function(e){
            //             console.log(e)
            //         }
            // })

      }

</script>


</body>
</html>